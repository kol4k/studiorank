<?php

namespace App\Helpers;

class Str {
    public static function num2char($num) {
        $num = intval($num);
        $char = '';
        if ($num > 0) {
            $char = chr($num + 64);
        }
        return $char;
    }

    public static function char2num($char) {
        $char = strtoupper($char);
        $num = 0;
        if (strlen($char) == 1) {
            $num = ord($char) - 64;
        }
        return $num;
    }
}