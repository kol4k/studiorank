<?php

namespace App\Imports\Company;

use App\Mail\User\ActivationAccountMail;
use App\Mail\User\InvitationMail;
use App\Models\Test;
use App\Models\TestUser;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class InvititationParticipantImport implements ToModel, WithHeadingRow, WithValidation
{
    use Importable;

    protected $test_id;

    public function __construct($test_id)
    {
        $this->test_id = $test_id;
    }

    /** Rules */
    public function rules(): array
    {
        return [
            // 'email' => 'required|email|unique:test_user,email,NULL,id,test_id,' . $this->test_id,
            // 'name' => 'required|string',
        ];
    }

    public function model(array $rows)
    {
        $user = User::where('email', $rows['email'])->first();
        $uuid = Str::uuid();
        
        $password = Str::random(8);
        if (!$user) {
            $user = User::create([
                'name' => $rows['name'],
                'email' => $rows['email'],
                'password' => Hash::make($password),
            ]);

            /** Send Mail ActivationAccountMail */
            Mail::to($user->email)->queue(new ActivationAccountMail($user, $password));
        }

        if (!$user->tests->contains($this->test_id)) {
            /** Check */
            $token = TestUser::where('token', $uuid)->first();

            if ($token) {
                $uuid = Str::uuid();
            }

            $test = Test::find($this->test_id);
            $test->participants()->attach($user->id, [
                'name' => $rows['name'],
                'email' => $rows['email'],
                'token' => $uuid,
                'created_at' => now(),
            ]);

            /** Send Mail InvitationMail */
            Mail::to($user->email)->queue(new InvitationMail($user, $test));
        }
    }
}