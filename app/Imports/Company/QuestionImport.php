<?php

namespace App\Imports\Company;

use App\Models\Module;
use App\Models\ModuleQuestionCategory;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class QuestionImport implements ToModel, WithHeadingRow, WithValidation
{
    use Importable;

    protected $module_id;
    protected $company_id;
    protected $created_by;
    protected $folder_name;

    public function __construct($module_id, $company_id, $created_by, $folder_name)
    {
        $this->module_id = $module_id;
        $this->company_id = $company_id;
        $this->created_by = $created_by;
        $this->folder_name = $folder_name;
    }

    /** Rules */
    public function rules(): array
    {
        return [
            // 'email' => 'required|email|unique:test_user,email,NULL,id,test_id,' . $this->test_id,
            // 'name' => 'required|string',
        ];
    }

    public function model(array $rows)
    {
        /** Create category first */
        $category = ModuleQuestionCategory::firstOrCreate([
            'name' => $rows['category'],
        ], [
            'company_id' => $this->company_id,
            'created_by' => $this->created_by
        ]);

        /** Create choices from row a,b,c,d,e,f,g,h,i,j and json_encode it */
        $choices = [];
        foreach (range('a', 'j') as $char) {
            if (isset($rows['choice_'.$char])) {
                /** Change to string */
                $choices[Str::upper($char)] = (string) $rows['choice_'.$char];
            }
        }
        
        $image = null;
        $imageType = null;
        $imagePath = isset($rows['image']) && $rows['image'] != '' ? $this->folder_name.'/'.$rows['image'] : '';
        if ($rows['image'] && Storage::disk('local')->get($imagePath)) {
            $image = base64_encode(Storage::disk('local')->get($imagePath));
            $imageType = Storage::disk('local')->mimeType($imagePath);
        }

        $module = Module::find($this->module_id);
        $module->questions()->create([
            'question' => $rows['question'],
            'choices' => (isset($choices) ? json_encode($choices) : null),
            'answer' => $rows['answer'],
            'question_image' => $image,
            'question_image_type' => $imageType,
            'category_id' => $category->id,
            'company_id' => $this->company_id,
            'created_by' => $this->created_by
        ]);
    }
}
