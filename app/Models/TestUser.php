<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestUser extends Model
{
    use HasFactory;

    protected $table = 'test_user';

    protected $fillable = [
        'test_id',
        'user_id',
        'name',
        'email',
        'token',
        'start_test',
        'end_test',
        'submit_test',
        'status',
        'number',
    ];

    protected $casts = [
        'start_test' => 'datetime',
        'end_test' => 'datetime',
        'submit_test' => 'datetime',
    ];

    public function test()
    {
        return $this->belongsTo(Test::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function questions()
    {
        return $this->hasMany(TestUserQuestion::class, 'test_user_id');
    }

    public function misbehaviors()
    {
        return $this->hasMany(MisbehaviorWindow::class, 'test_user_id');
    }

    public function scopeByUser($query)
    {
        return $query->where('user_id', auth()->guard('user')->id());
    }

    public function scopeByPublished($query)
    {
        return $query->whereHas('test', function ($query) {
            $query->where('status', 1);
        });
    }
}
