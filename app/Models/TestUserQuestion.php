<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class TestUserQuestion extends Model
{
    use HasFactory;

    protected $table = 'test_user_question';

    protected $fillable = [
        'test_user_id',
        'module_question_id',
        'answer',
        'is_correct',
        'camera_image'
    ];

    public function testUser()
    {
        return $this->belongsTo(TestUser::class, 'test_user_id');
    }

    public function question()
    {
        return $this->belongsTo(ModuleQuestion::class, 'module_question_id');
    }

    public function getImgCameraBase64Attribute()
    {
        $pathFile = $this->camera_image;
        if ($pathFile) {
            $file = Storage::get($pathFile);
            $extFile = pathinfo($pathFile, PATHINFO_EXTENSION);
            return 'data:image/'.$extFile.';base64,'.base64_encode($file);
        }
    }
}
