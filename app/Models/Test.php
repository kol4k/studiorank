<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use HasFactory;

    protected $table = 'tests';

    protected $casts = [
        'start_test' => 'datetime',
        'end_test' => 'datetime',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function modules()
    {
        return $this->belongsToMany(Module::class, 'module_test', 'test_id', 'module_id')->withTimestamps();
    }

    public function participants()
    {
        return $this->belongsToMany(User::class, 'test_user', 'test_id', 'user_id')->withPivot(['id', 'start_test', 'end_test', 'submit_test', 'status', 'number', 'file_report'])->withTimestamps();
    }

    public function scopeByCompany($query)
    {
        $query->where('company_id', auth()->id());
    }

    public function scopeByUser($query)
    {
        $query->whereHas('participants', function ($query) {
            $query->where('user_id', auth()->guard('user')->id());
        });
    }

    public function scopeByPublished($query)
    {
        $query->where('status', 1);
    }

    public function getCountQuestionAttribute()
    {
        $count = 0;
        foreach ($this->modules as $module) {
            $count += $module->questions->count();
        }
        return $count;
    }
}
