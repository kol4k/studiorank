<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MisbehaviorWindow extends Model
{
    use HasFactory;

    protected $table = 'misbehavior_window';

    protected $fillable = [
        'test_user_id',
        'user_id',
        'session',
        'status',
        'time'
    ];
}
