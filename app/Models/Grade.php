<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    use HasFactory;

    protected $table = 'grades';

    public function group()
    {
        return $this->belongsTo(GradeGroup::class, 'group_id', 'id');
    }

    public function scopeByCompany($query)
    {
        $query->where('company_id', auth()->id());
    }
}
