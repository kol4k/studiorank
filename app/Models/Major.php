<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Major extends Model
{
    use HasFactory;

    public $table = 'majors';

    protected $fillable = ['name', 'is_active'];

    public function scopeByActive($query, $status = true)
    {
        return $query->where('is_active', $status);
    }
}
