<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use HasFactory;

    protected $table = 'modules';

    public function groupGrade()
    {
        return $this->belongsTo(GradeGroup::class, 'grade_group_id');
    }

    public function questions()
    {
        return $this->hasMany(ModuleQuestion::class, 'module_id');
    }

    public function scopeByCompany($query)
    {
        $query->where('company_id', auth()->id());
    }
}
