<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModuleQuestion extends Model
{
    use HasFactory;

    protected $table = 'module_questions';

    public $fillable = [
        'question',
        'question_image',
        'question_image_type',
        'choices',
        'answer',
        'point_plus',
        'point_min',
        'category_id',
        'module_id',
        'company_id',
        'created_by'
    ];

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function category()
    {
        return $this->belongsTo(ModuleQuestionCategory::class, 'category_id');
    }

    public function scopeByCompany($query)
    {
        $query->where('company_id', auth()->id());
    }

    public function scopeByModule($query, $value)
    {
        $query->where('module_id', $value);
    }

    public function getShowImageAttribute()
    {
        if ($this->attributes['question_image'] && $this->attributes['question_image_type']) {
            // Convert bytea to string from question_image
            
            $image = 'data:'.$this->attributes['question_image_type'].';base64,'.$this->attributes['question_image'];
        } else {
            $image = null;
        }
        return $image;
    }
}
