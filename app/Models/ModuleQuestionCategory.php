<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModuleQuestionCategory extends Model
{
    use HasFactory;

    protected $table = 'module_question_categories';

    public $fillable = [
        'name',
        'company_id',
        'created_by'
    ];

    public function scopeByCompany($query)
    {
        $query->where('company_id', auth()->id());
    }
}
