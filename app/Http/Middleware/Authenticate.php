<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        /** Midleware guard company redirect to company login */
        if (! $request->expectsJson()) {
            if ($request->is('company/*') || $request->getHost() == 'admin.studiorank.xyz' || ($request->route()->getName() && strpos($request->route()->getName(), 'company.') === 0)) {
                return route('company.auth.login');
            } elseif ($request->is('/*') || $request->getHost() == 'app.studiorank.xyz' || ($request->route()->getName() && strpos($request->route()->getName(), 'user.') === 0)) {
                return route('user.auth.login');
            }
        }

        // if (! $request->expectsJson()) {
        //     return route('login');
        // }
    }
}
