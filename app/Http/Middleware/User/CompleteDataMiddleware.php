<?php

namespace App\Http\Middleware\User;

use Closure;
use Illuminate\Http\Request;

class CompleteDataMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (!auth()->guard('user')->user()->detail()->exists()) {
            return redirect()->route('user.setting.profile');
        }
        return $next($request);
    }
}