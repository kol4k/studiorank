<?php

namespace App\Http\Middleware\User;

use App\Models\TestUser;
use Closure;
use Illuminate\Http\Request;

class TestHasFinishedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->route('token');
        $testUser = TestUser::byUser()->where('token', $token)->firstOrFail();

        if ($testUser->status == 3) {
            return redirect()->route('user.test.detail', ['token' => $token]);
        } elseif ($testUser->test->start_test > now()) {
            return redirect()->route('user.test.detail', ['token' => $token])->with('error', 'The test hasn\'t started yet');
        } elseif ($testUser->test->end_test < now()) {
            $this->updateStatus($testUser);
            return redirect()->route('user.test.detail', ['token' => $token])->with('error', 'The test has ended');
        }

        return $next($request);
    }

    public function updateStatus($testUser)
    {
        if ($testUser->status == 1 || $testUser->status == 2) {
            $testUser->status = 3;
            $testUser->submit_test = now();
            $testUser->save();
        }
    }
}