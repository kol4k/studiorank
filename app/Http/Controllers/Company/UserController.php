<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Models\Grade;
use App\Models\GradeGroup;
use App\Models\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index()
    {
        $users = User::byCompany()->get();
        return view('company.user.index', [
            'users' => $users,
        ]);
    }

    public function create()
    {
        return view('company.user.create');
    }
}
