<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Models\Grade;
use App\Models\GradeGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GradeController extends Controller
{
    public function attributes()
    {
        return [
            'group' => 'Nama Group',
            'name' => 'Tampilan Peringkat',
            'value_start' => 'Nilai Awal',
            'value_end' => 'Nilai Akhir',
            'result' => 'Hasil Peringkat',
            'color' => 'Warna Peringkat'
        ];
    }

    public function index()
    {
        $grades = Grade::byCompany()->get();

        return view('company.grade.index', [
            'grades' => $grades
        ]);
    }

    public function create()
    {
        $groups = GradeGroup::byCompany()->get();
        return view('company.grade.create', [
            'groups' => $groups
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'group' => 'required',
            'name' => 'required',
            'value_start' => 'required|numeric|lt:value_end',
            'value_end' => 'required|numeric|gt:value_start',
            'result' => 'required',
            'color' => 'required|in:red,yellow,orange,green,blue'
        ], [], $this->attributes());
        
        DB::beginTransaction();
        try {
            $group = GradeGroup::firstOrCreate([
                'name' => json_decode($request->group)[0]->value,
                'company_id' => auth()->guard('company')->user()->company_id
            ], [
                'created_by' => auth()->guard('company')->user()->id
            ]);

            $grade = new Grade();
            $grade->group_id = $group->id;
            $grade->value_start = $request->value_start;
            $grade->value_end = $request->value_end;
            $grade->name = $request->name;
            $grade->result = $request->result;
            $grade->color = $request->color;
            $grade->created_by = auth()->guard('company')->id();
            $grade->company_id = auth()->guard('company')->user()->company_id;
            $grade->save();

            DB::commit();
            return redirect()->route('company.grade.index')->with('success', 'Berhasil menambahkan data peringkat');
        } catch (\Throwable $th) {
            DB::rollback();
            dd($th->getMessage());
            return redirect()->back()->with('error', 'Gagal menambahkan data peringkat');
        }
    }

    public function edit($id)
    {
        $grade = Grade::byCompany()->findOrFail($id);
        $groups = GradeGroup::byCompany()->get();
        return view('company.grade.edit', [
            'grade' => $grade,
            'groups' => $groups
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'group' => 'required',
            'name' => 'required',
            'value_start' => 'required|numeric|lt:value_end',
            'value_end' => 'required|numeric|gt:value_start',
            'result' => 'required',
            'color' => 'required|in:red,yellow,orange,green,blue'
        ], [], $this->attributes());
        
        DB::beginTransaction();
        try {
            $group = GradeGroup::firstOrCreate([
                'name' => json_decode($request->group)[0]->value,
                'company_id' => auth()->guard('company')->user()->company_id
            ], [
                'created_by' => auth()->guard('company')->user()->id
            ]);

            $grade = Grade::byCompany()->findOrFail($id);
            $grade->group_id = $group->id;
            $grade->value_start = $request->value_start;
            $grade->value_end = $request->value_end;
            $grade->name = $request->name;
            $grade->result = $request->result;
            $grade->color = $request->color;
            $grade->save();

            DB::commit();
            return redirect()->route('company.grade.index')->with('success', 'Berhasil mengubah data peringkat');
        } catch (\Throwable $th) {
            DB::rollback();
            dd($th->getMessage(), $request->all());
            return redirect()->back()->with('error', 'Gagal mengubah data peringkat');
        }
    }

    public function destroy($id)
    {
        $grade = Grade::byCompany()->findOrFail($id);
        $grade->delete();

        return redirect()->route('company.grade.index')->with('success', 'Berhasil menghapus data peringkat');
    }
}
