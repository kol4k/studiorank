<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login()
    {
        return view('company.auth.login');
    }

    public function loginProcess(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if (auth()->guard('company')->attempt($credentials)) {
            return redirect()->route('company.dashboard');
        }

        return redirect()->back()->withErrors([
            'email' => 'Email atau password salah',
        ]);
    }

    public function logout()
    {
        auth()->guard('company')->logout();

        return redirect()->route('company.auth.login');
    }
}
