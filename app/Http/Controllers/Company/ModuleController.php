<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Models\Grade;
use App\Models\GradeGroup;
use App\Models\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ModuleController extends Controller
{
    public function attributes()
    {
        return [
            'nama' => 'Nama Modul',
            'type' => 'Tipe',
            'description' => 'Deskripsi',
            'grade' => 'Tampilan Peringkat',
            'is_publish' => 'Status Menampilkan',
            'is_global' => 'Akses Umum'
        ];
    }

    public function index()
    {
        $modules = Module::byCompany()->get();

        return view('company.module.index', [
            'modules' => $modules
        ]);
    }

    public function show($id)
    {
        $module = Module::byCompany()->findOrFail($id);
        $gradesGroup = GradeGroup::byCompany()->get();

        return view('company.module.show', [
            'module' => $module,
            'gradesGroup' => $gradesGroup
        ]);
    }

    public function create()
    {
        $modules = Module::byCompany()->get();
        $gradesGroup = GradeGroup::byCompany()->get();

        return view('company.module.create', [
            'modules' => $modules,
            'gradesGroup' => $gradesGroup
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'nullable|string',
            'grade' => 'required|exists:grades_group,id',
            'is_publish' => 'required|in:1,0'
        ], [], $this->attributes());

        DB::beginTransaction();
        try {
            $module = new Module();
            $module->name = $request->name;
            $module->description = $request->description;
            $module->grade_group_id = $request->grade;
            $module->is_publish = $request->is_publish;
            $module->type = 'test';
            $module->company_id = auth()->user()->company_id;
            $module->created_by = auth()->user()->id;
            $module->save();

            DB::commit();
            return redirect()->route('company.module.index')->with('success', 'Modul berhasil ditambahkan');
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Modul gagal ditambahkan');
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'nullable|string',
            'grade' => 'required|exists:grades_group,id',
            'is_publish' => 'required|in:1,0'
        ], [], $this->attributes());

        DB::beginTransaction();
        try {
            $module = Module::byCompany()->findOrFail($id);
            $module->name = $request->name;
            $module->description = $request->description;
            $module->grade_group_id = $request->grade;
            $module->is_publish = $request->is_publish;
            $module->type = 'test';
            $module->company_id = auth()->user()->company_id;
            $module->save();

            DB::commit();
            return redirect()->back()->with('success', 'Modul berhasil diubah');
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Modul gagal diubah');
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $module = Module::byCompany()->findOrFail($id);
            $module->delete();

            DB::commit();
            return redirect()->back()->with('success', 'Modul berhasil dihapus');
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Modul gagal dihapus');
        }
    }
}
