<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Imports\Company\InvititationParticipantImport;
use App\Mail\User\ActivationAccountMail;
use App\Mail\User\InvitationMail;
use App\Models\Test;
use App\Models\TestUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class InvititationParticipantController extends Controller
{
    public function attributes()
    {
        return [
            'name' => 'Nama Peserta',
            'email' => 'Email',
            'file' => 'Berkas',
        ];
    }

    public function createSingle($id)
    {
        $test = Test::findOrFail($id);
        return view('company.test.participant.create', [
            'test' => $test,
        ]);
    }

    public function storeSingle(Request $request, $id)
    {
        $test = Test::findOrFail($id);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:test_user,email,NULL,id,test_id,' . $test->id,
        ]);

        DB::beginTransaction();
        try {
            $password = Str::random(8);
            $user = User::where('email', $request->email)->first();
            $uuid = Str::uuid();

            if (!$user) {
                $user = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => Hash::make($password),
                ]);

                /** Send Mail ActivationAccountMail */
                Mail::to($user->email)->queue(new ActivationAccountMail($user, $password));
            }
            /** Check */
            $token = TestUser::where('token', $uuid)->first();

            if ($token) {
                $uuid = Str::uuid();
            }

            // create test_user
            $test->participants()->attach($user->id, [
                'name' => $request->name,
                'email' => $request->email,
                'token' => $uuid,
                'created_at' => now(),
            ]);

            /** Send Mail InvitationMail */
            Mail::to($user->email)->queue(new InvitationMail($user, $test));
    
            DB::commit();
            return redirect()->route('company.test.show', $test->id)->with('success', 'Berhasil menambahkan peserta');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error', 'Gagal menambahkan peserta');
        }
    }

    public function createMultiple($id)
    {
        $test = Test::findOrFail($id);
        return view('company.test.participant.create-multiple', [
            'test' => $test,
        ]);
    }

    public function storeMultiple(Request $request, $id)
    {        
        $this->validate($request, [
            'file' => 'required|mimes:xlsx,xls',
        ]);

        try {
            $test = Test::findOrFail($id);
            Excel::import(new InvititationParticipantImport($test->id), $request->file('file'));
            return redirect()->route('company.test.show', $test->id)->with('success', 'Berhasil menambahkan peserta');
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            $errors = [];
            foreach ($failures as $failure) {
                $errors[] = 'Row ' . $failure->row() . ' - ' . $failure->errors()[0];
            }
            return redirect()->back()->with('error', $errors);
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal menambahkan peserta');
        }
    }
}
