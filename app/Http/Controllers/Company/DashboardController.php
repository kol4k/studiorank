<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Models\Test;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $tests = Test::byCompany()->get();
        $notWorkingTest = Test::byCompany()->whereHas('participants', function ($query) {
            $query->where('status', '1');
        })->get();
        $onGoingTest = Test::byCompany()->whereHas('participants', function ($query) {
            $query->where('status', '2');
        })->get();
        $doneTest = Test::byCompany()->whereHas('participants', function ($query) {
            $query->where('status', '3');
        })->get();

        return view('company.dashboard', [
            'tests' => $tests,
            'notWorkingTest' => $notWorkingTest,
            'onGoingTest' => $onGoingTest,
            'doneTest' => $doneTest,
        ]);
    }
}
