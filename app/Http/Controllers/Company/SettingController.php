<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function account()
    {
        return view('company.setting.account');
    }

    public function accountUpdate(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
        ]);

        auth()->guard('company')->user()->update([
            'name' => $request->name,
        ]);

        return redirect()->back()->with('success', 'Berhasil mengubah data akun');
    }

    public function passwordUpdate(Request $request)
    {
        $request->validate([
            'new_password' => 'required|string|min:8|confirmed',
        ]);

        if (!password_verify($request->old_password, auth()->guard('company')->user()->new_password)) {
            return redirect()->back()->with('error', 'Password lama tidak sesuai');
        }
        
        auth()->guard('company')->user()->update([
            'password' => bcrypt($request->new_password),
        ]);

        return redirect()->back()->with('success', 'Berhasil mengubah password');
    }

    public function company(Request $request)
    {
        return view('company.setting.company');
    }

    public function companyUpdate(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
        ]);

        auth()->guard('company')->user()->company->update([
            'name' => $request->name,
        ]);

        return redirect()->back()->with('success', 'Berhasil mengubah data perusahaan');
    }
}
