<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Imports\Company\QuestionImport;
use App\Models\Grade;
use App\Models\GradeGroup;
use App\Models\Module;
use App\Models\ModuleQuestion;
use App\Models\ModuleQuestionCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use ZanySoft\Zip\Zip;
use ZipArchive;

class QuestionController extends Controller
{
    public function attributes()
    {
        return [
            'file' => 'File',
            'category' => 'Kategori',
            'image' => 'Gambar Soal',
            'question' => 'Pertanyaan',
            'answer' => 'Jawaban',
        ];
    }

    public function createSingle($id)
    {
        $module = Module::find($id);
        $categories = ModuleQuestionCategory::byCompany()->get();
        return view('company.module.question.create', [
            'module' => $module,
            'categories' => $categories,
        ]);
    }

    public function storeSingle(Request $request, $id)
    {
        $this->validate($request, [
            'category' => 'required',
            'image' => 'nullable|image',
            'question' => 'required',
            'answer.*.choice' => 'required',
        ], [], $this->attributes());

        DB::beginTransaction();
        try {
            /** Category */
            $category = ModuleQuestionCategory::firstOrCreate([
                'name' => json_decode($request->category)[0]->value,
            ], [
                'company_id' => auth()->guard('company')->user()->company_id,
                'created_by' => auth()->guard('company')->user()->id
            ]);

            $choices = [];
            $answer = null;
            $iAnswer = 0;
            foreach ($request->answers as $dataAnswer) {
                $choices[Str::upper(chr(ord('a') + $iAnswer))] = $dataAnswer['choice'];
                if (isset($dataAnswer['answer'])) {
                    // 2 to alphabet
                    $answer = Str::upper(chr(ord('a') + $iAnswer));
                }
                $iAnswer++;
            }
            $choices = json_encode($choices);

            /** Image: get image from request file and convert it to base64 without extension file, extension file save to question_image_type */
            $image = null;
            $imageType = null;
            if ($request->hasFile('image')) {
                $image = base64_encode(file_get_contents($request->file('image')));
                $imageType = $request->file('image')->getMimeType();
            }

            $module = Module::byCompany()->findOrFail($id);
            $module->questions()->create([
                'question' => $request->question,
                'choices' => (isset($choices) ? $choices : null),
                'answer' => $answer,
                'question_image' => $image,
                'question_image_type' => $imageType,
                'category_id' => $category->id,
                'created_by' => auth()->guard('company')->user()->id,
                'company_id' => auth()->guard('company')->user()->company_id
            ]);
            DB::commit();
            return redirect()->route('company.module.show', $id)->with('success', 'Soal berhasil ditambahkan');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error', 'Soal gagal ditambahkan');
        }
    }

    public function createMultiple($id)
    {
        $module = Module::find($id);
        return view('company.module.question.create-multiple', [
            'module' => $module,
        ]);
    }

    public function storeMultiple(Request $request, $id)
    {        
        $this->validate($request, [
            // Mime Zip
            'file' => 'required|mimes:zip',
        ]);

        try {
            $module = Module::byCompany()->findOrFail($id);
            $folderName = 'questions/'.Str::uuid();
            $fileUpload = $request->file('file')->storePubliclyAs($folderName, 'file.zip', 'local');
            $zipFile = Zip::open(Storage::disk('local')->path($fileUpload));

            if ($zipFile->has('template_question.xlsx', ZipArchive::FL_NOCASE|ZipArchive::FL_NODIR)) {
                $pathName = Str::remove('file.zip', Storage::disk('local')->path($fileUpload));
                $zipFile->extract($pathName);
                $zipFile->close();
                Excel::import(new QuestionImport($module->id, auth()->guard('company')->user()->company_id, auth()->guard('company')->user()->id, $folderName), Storage::disk('local')->path($folderName.'/template_question.xlsx'));

                return redirect()->route('company.module.show', $id)->with('success', 'Soal berhasil ditambahkan');
            } else {
                return redirect()->back()->with('error', 'File tidak valid');
            }
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            $errors = [];
            foreach ($failures as $failure) {
                $errors[] = 'Row ' . $failure->row() . ' - ' . $failure->errors()[0];
            }
            return redirect()->back()->with('error', $errors);
        } catch (\Throwable $th) {
            dd($th);
            return redirect()->back()->with('error', 'Gagal menambahkan soal');
        }
    }

    public function edit($id)
    {
        $question = ModuleQuestion::byCompany()->findOrFail($id);
        $categories = ModuleQuestionCategory::byCompany()->get();
        return view('company.module.question.edit', [
            'question' => $question,
            'categories' => $categories,
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category' => 'required',
            'image' => 'nullable|image',
            'question' => 'required',
            'answer.*.choice' => 'required',
        ], [], $this->attributes());

        DB::beginTransaction();
        try {
            /** Category */
            $category = ModuleQuestionCategory::firstOrCreate([
                'name' => json_decode($request->category)[0]->value,
            ], [
                'company_id' => auth()->guard('company')->user()->company_id,
                'created_by' => auth()->guard('company')->user()->id
            ]);

            $choices = [];
            $answer = null;
            $iAnswer = 0;
            foreach ($request->answers as $dataAnswer) {
                $choices[Str::upper(chr(ord('a') + $iAnswer))] = (string) $dataAnswer['choice'];
                if (isset($dataAnswer['answer'])) {
                    // 2 to alphabet
                    $answer = Str::upper(chr(ord('a') + $iAnswer));
                }
                $iAnswer++;
            }
            $choices = json_encode($choices);

            /** Image: get image from request file and convert it to base64 without extension file, extension file save to question_image_type */
            if ($request->hasFile('image')) {
                $image = null;
                $imageType = null;
                if ($request->hasFile('image')) {
                    $image = base64_encode(file_get_contents($request->file('image')));
                    $imageType = $request->file('image')->getMimeType();
                }
            }

            $question = ModuleQuestion::byCompany()->findOrFail($id);
            $question->update([
                'question' => $request->question,
                'choices' => (isset($choices) ? $choices : null),
                'answer' => $answer,
                'question_image' => ($request->hasFile('image') ? $image : $question->question_image),
                'question_image_type' => ($request->hasFile('image') ? $imageType : $question->question_image_type),
                'category_id' => $category->id,
                'updated_by' => auth()->guard('company')->user()->id,
            ]);
            DB::commit();
            return redirect()->route('company.module.show', $question->module_id)->with('success', 'Soal berhasil diubah');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error', 'Soal gagal diubah');
        }
    }

    public function destroy($id)
    {
        $question = ModuleQuestion::byCompany()->findOrFail($id);
        $question->delete();
        return redirect()->back()->with('success', 'Soal berhasil dihapus');
    }
}
