<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Models\Grade;
use App\Models\GradeGroup;
use App\Models\Module;
use App\Models\Test;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function attributes()
    {
        return [
            'name' => 'Nama Tes',
            'description' => 'Keterangan',
            'start_test' => 'Waktu mulai tes',
            'end_test' => 'Waktu selesai tes',
            'camera_supervision' => 'Supervisi kamera',
            'status' => 'Status',
            'email_invitation_custom' => 'Custom email undangan',
            'email_invitation_title' => 'Judul email undangan',
            'email_invitation_content' => 'Isi email undangan',
        ];
    }

    public function index()
    {
        $tests = Test::byCompany()->get();
        return view('company.test.index', [
            'tests' => $tests,
        ]);
    }

    public function show($id)
    {
        $test = Test::byCompany()->findOrFail($id);
        
        return view('company.test.show', [
            'test' => $test,
        ]);
    }

    public function create()
    {
        $modules = Module::byCompany()->get();

        return view('company.test.create', [
            'modules' => $modules,
        ]);
    }

    public function store(Request $request)
    {
        $implodeDatetime = explode(" - ", $request->date);
        $request->merge([
            'start_test' => Carbon::createFromFormat('d/m/Y H:i', $implodeDatetime[0])->format('d-m-Y H:i:s'),
            'end_test' => Carbon::createFromFormat('d/m/Y H:i', $implodeDatetime[1])->format('d-m-Y H:i:s'),
        ]);

        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'start_test' => 'required|date_format:d-m-Y H:i:s|before:end_test',
            'end_test' => 'required|date_format:d-m-Y H:i:s|after:start_test',
            'modules' => 'required',
            'camera_supervision' => 'nullable|boolean',
            // 'email_invitation_custom' => 'nullable|boolean',
            // 'email_invitation_title' => 'required_if:email_invitation_custom,1',
            // 'email_invitation_content' => 'required_if:email_invitation_custom,1',
        ], [], $this->attributes());

        DB::beginTransaction();
        try {
            $test = new Test();
            $test->name = $request->name;
            $test->description = $request->description;
            $test->start_test = $request->start_test;
            $test->end_test = $request->end_test;
            $test->camera_supervision = $request->camera_supervision ?? 0;
            $test->status = $request->status ?? 0;
            $test->email_invitation_custom = $request->email_invitation_custom ?? 0;
            // $test->email_invitation_title = $request->email_invitation_title;
            // $test->email_invitation_content = $request->email_invitation_content;
            $test->created_by = auth()->guard('company')->user()->id;
            $test->company_id = auth()->guard('company')->user()->company_id;
            $test->save();

            $test->modules()->attach($request->modules);

            DB::commit();
            return redirect()->route('company.test.index')->with('success', 'Berhasil menambahkan data tes');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error', 'Gagal menambahkan data tes');
        }
    }

    public function edit($id)
    {
        $test = Test::byCompany()->findOrFail($id);
        $modules = Module::byCompany()->get();

        return view('company.test.edit', [
            'test' => $test,
            'modules' => $modules,
        ]);
    }

    public function update(Request $request, $id)
    {
        $implodeDatetime = explode(" - ", $request->date);
        $request->merge([
            'start_test' => Carbon::createFromFormat('d/m/Y H:i', $implodeDatetime[0])->format('d-m-Y H:i:s'),
            'end_test' => Carbon::createFromFormat('d/m/Y H:i', $implodeDatetime[1])->format('d-m-Y H:i:s'),
        ]);

        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'start_test' => 'required|date_format:d-m-Y H:i:s|before:end_test',
            'end_test' => 'required|date_format:d-m-Y H:i:s|after:start_test',
            'modules' => 'required',
            'camera_supervision' => 'nullable|boolean',
            // 'email_invitation_custom' => 'nullable|boolean',
            // 'email_invitation_title' => 'required_if:email_invitation_custom,1',
            // 'email_invitation_content' => 'required_if:email_invitation_custom,1',
        ], [], $this->attributes());

        DB::beginTransaction();
        try {
            $test = Test::byCompany()->findOrFail($id);
            $test->name = $request->name;
            $test->description = $request->description;
            $test->start_test = $request->start_test;
            $test->end_test = $request->end_test;
            $test->camera_supervision = $request->camera_supervision ?? 0;
            $test->status = $request->status ?? 0;
            $test->email_invitation_custom = $request->email_invitation_custom ?? 0;
            // $test->email_invitation_title = $request->email_invitation_title;
            // $test->email_invitation_content = $request->email_invitation_content;
            $test->save();

            $test->modules()->sync($request->modules);

            DB::commit();
            return redirect()->route('company.test.show', $test->id)->with('success', 'Berhasil mengubah data tes');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            return redirect()->back()->with('error', 'Gagal mengubah data tes');
        }
    }
}
