<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Major;
use App\Models\Province;
use App\Models\School;
use App\Models\UserCandidateDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class FileController extends Controller
{
    public function getFile(Request $request)
    {
        $pathFile = $request->path;
        // Check storage has connect
        if (Storage::exists($pathFile)) {
            $file = Storage::get($pathFile);
            $extFile = pathinfo($pathFile, PATHINFO_EXTENSION);
            if ($extFile == 'pdf') {
                $type = 'application/pdf';
            } else {
                $type = 'image/'.$extFile;
            }
            return response()->make($file, 200, [
                'Content-Type' => $type
            ]);
        }
        abort(404);
    }
}
