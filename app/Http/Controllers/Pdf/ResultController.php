<?php

namespace App\Http\Controllers\Pdf;

use App\Helpers\Str;
use App\Http\Controllers\Controller;
use App\Models\Grade;
use App\Models\MisbehaviorWindow;
use App\Models\TestUser;
use App\Models\TestUserQuestion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Image;
use Spatie\Browsershot\Browsershot;

class ResultController extends Controller
{
    public function result($token)
    {
        $testUser = TestUser::with(['questions'])->where('token', $token)->firstOrFail();
        $result = $testUser->questions->groupBy('question.module.name');
        foreach ($result as $key => $value) {
            $correct = $value->where('is_correct', 1)->count();
            $incorrect = $value->where('is_correct', 0)->count();
            $gradeByModuleGroupGrade = Grade::where('group_id', $value->first()->question->module->grade_group_id)->where([['value_start', '<=', $correct], ['value_end', '>=', $correct]])->first();
            if ($gradeByModuleGroupGrade->color == 'red') {
                $gradeColor = 'background-color: #FF0000;';
            } else if ($gradeByModuleGroupGrade->color == 'yellow') {
                $gradeColor = 'background-color: #FFFF04; color: black;';
            } else if ($gradeByModuleGroupGrade->color == 'green') {
                $gradeColor = 'background-color: #00FF00; color: dark;';
            } else if ($gradeByModuleGroupGrade->color == 'orange') {
                $gradeColor = 'background-color: #FF9902;';
            } else {
                $gradeColor = 'background-color: #1255CC;';
            }

            $result[$key] = [
                'correct' => $correct,
                'incorrect' => $incorrect,
                'total' => $correct + $incorrect,
                'grade_name' => $gradeByModuleGroupGrade->name,
                'grade_description' => $gradeByModuleGroupGrade->result,
                'grade_color' => $gradeColor,
            ];
        }
        $misbehaviorsWindow = $testUser->misbehaviors->groupBy('session')->map(function ($data) {
            return Carbon::parse($data[0]->time)->diffInSeconds((isset($data[1]) ? $data[1]->time : $data[0]->time));
        });

        $misbehaviorWindow = [
            0 => 0,
            1 => 0,
            2 => 0,
            3 => 0,
        ];

        foreach ($misbehaviorsWindow as $time) {
            if ($time >= '15') { $misbehaviorWindow[0] = $misbehaviorWindow[0] + 1; }
            elseif ($time <= '16' && $time >= '30') { $misbehaviorWindow[1] = $misbehaviorWindow[1] + 1; }
            elseif ($time <= '31' && $time >= '60') { $misbehaviorWindow[2] = $misbehaviorWindow[2] + 1; }
            elseif ($time <= '61') { $misbehaviorWindow[3] = $misbehaviorWindow[3] + 1; }
        }

        return view('pdf.result', [
            'testUser' => $testUser,
            'misbehaviorWindow' => $misbehaviorWindow,
            'result' => $result,
        ]);
    }

    public function downloadByBrowsershot($token)
    {
        $testUser = TestUser::where('token', $token)->firstOrFail();
        $fileNamePDF = \Illuminate\Support\Str::uuid().'.pdf';

        $generate = Browsershot::url(Route('result', $token))
            ->setNodeBinary('/Users/hacker/.nvm/versions/node/v18.1.0/bin/node')
            ->setNpmBinary('/Users/hacker/.nvm/versions/node/v18.1.0/bin/npm')
            ->setNodeModulePath('/Users/hacker/Projects/studio/node_modules')
            ->setChromePath("/usr/bin/chromium-browser")
            ->addChromiumArguments([
                '--disable-web-security', '--no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage'
            ])
            ->noSandbox()->ignoreHttpsErrors()
            ->waitUntilNetworkIdle()
            ->emulateMedia('screen')
            ->showBackground()
            ->format('A4')
            ->setOption('args', ['--disable-web-security', '--no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage'])
            ->save(storage_path() . '/app/public/result/'.$fileNamePDF);
    }
}
