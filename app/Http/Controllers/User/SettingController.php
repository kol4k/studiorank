<?php

namespace App\Http\Controllers\User;

use App\Helpers\Str;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Major;
use App\Models\Province;
use App\Models\School;
use App\Models\UserDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function profile()
    {
        $cities = City::orderBy('created_at', 'ASC')->get();
        $provinces = Province::orderBy('name', 'ASC')->get();
        $majors = Major::orderBy('created_at', 'ASC')->get();
        $schools = School::orderBy('name', 'ASC')->get();

        return view('user.setting.profile', [
            'cities' => $cities,
            'provinces' => $provinces,
            'majors' => $majors,
            'schools' => $schools
        ]);
    }
    
    public function profileProcess(Request $request)
    {
        $degreeValue = isset(json_decode($request->degree)[0]) ? json_decode($request->degree)[0]->value : null;
        $majorValue = isset(json_decode($request->major)[0]) ? json_decode($request->major)[0]->value : null;
        $schoolValue = isset(json_decode($request->school)[0]) ? json_decode($request->school)[0]->value : null;

        $validationArray = [
            'dob' => 'required|date_format:d/m/Y',
            'phone' => 'required|numeric|min:8',
            'gender' => 'required|in:Laki-laki,Perempuan',
            'province' => 'required|exists:\App\Models\Province,id',
            'address' => 'required',
            'city' => 'required|exists:\App\Models\City,id',
            'degree' => 'required|in:' . implode(',', array_keys((new UserDetail())->dataEducation())),
            'major' => 'required',
            'school' => 'required'
        ];

        $this->validate($request->merge([
            'degree' => $degreeValue,
            'major' => $majorValue,
            'school' => $schoolValue
        ]), $validationArray);

        DB::beginTransaction();
        try {
            $getUser = UserDetail::where('user_id', Auth::guard('user')->id())->first();
            $userDetail = isset($getUser) ? $getUser : new UserDetail();
            $userDetail->user_id = Auth::guard('user')->id();
            $userDetail->dob = Carbon::createFromFormat('d/m/Y', $request->dob)->format('Y-m-d');
            $userDetail->phone = $request->phone;
            $userDetail->gender = $request->gender;
            $userDetail->province_id = $request->province;
            $userDetail->city_id = $request->city;
            $userDetail->address = $request->address;
            $userDetail->degree = $degreeValue;
            $userDetail->major = $majorValue;
            $userDetail->school = $schoolValue;
            $userDetail->save();

            $returnRedirect = isset($getUser) ? redirect()->back()->with('success', 'Berhasil memperbaharui profil') : redirect()->route('user.test.index')->with('firstSetupProfile', true);

            DB::commit();

            return $returnRedirect;
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }
}
