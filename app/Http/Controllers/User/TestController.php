<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Test;
use App\Models\TestUser;
use App\Models\TestUserQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function index()
    {
        $testsUser = TestUser::byUser()->byPublished()->get();

        return view('user.test.index', [
            'testsUser' => $testsUser,
        ]);
    }

    public function detail($token)
    {
        $testUser = TestUser::byUser()->where('token', $token)->firstOrFail();
        return view('user.test.detail', [
            'testUser' => $testUser,
        ]);
    }

    public function start($token)
    {
        $testUser = TestUser::byUser()->where('token', $token)->first();
        return view('user.test.start', [
            'testUser' => $testUser,
        ]);
    }
    
    public function startProcess(Request $request, $token)
    {
        DB::beginTransaction();
        try {
            $testUser = TestUser::byUser()->where('token', $token)->firstOrFail();

            if (count($testUser->questions) == 0) {
                /** Get Question */
                $questions = [];
                foreach ($testUser->test->modules as $module) {
                    foreach ($module->questions as $question) {
                        $questions[] = $question;
                    }
                }
    
                if (count($questions) > 0) {
                    /** Random */
                    shuffle($questions);

                    /** Insert Question */
                    foreach ($questions as $question) {
                        $testUserQuestion = new TestUserQuestion();
                        $testUserQuestion->test_user_id = $testUser->id;
                        $testUserQuestion->module_question_id = $question->id;
                        $testUserQuestion->save();
                    }
                }
            }

            if ($testUser->status == 1) {
                $testUser->start_test = now();
                $testUser->status = 2;
                $testUser->save();
            }

            DB::commit();
            return redirect()->route('user.test.base', $testUser->token);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error', $th->getMessage());
        }
    }

    public function base($id)
    {
        $testUser = TestUser::byUser()->where('token', $id)->firstOrFail();
        return view('user.test.base', [
                'testUser' => $testUser,
        ]);
    }

    public function baseProcess(Request $request, $token)
    {
        DB::beginTransaction();
        try {
            $testUser = TestUser::byUser()->where('token', $token)->firstOrFail();
            $testUser->end_test = now();
            $testUser->submit_test = now();
            $testUser->status = 3;
            $testUser->save();

            DB::commit();
            return redirect()->route('user.test.result', $testUser->token)->with('success', 'Test Berhasil Di Submit');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('error', $th->getMessage());
        }
    }

    public function result($token)
    {
        $testUser = TestUser::byUser()->where('token', $token)->firstOrFail();
        return view('user.test.result', [
            'testUser' => $testUser,
        ]);
    }
}
