<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login()
    {
        if (Auth::guard('user')->check()) {
            return redirect()->route('user.test.index');
        }
        return view('user.auth.login');
    }

    public function loginProcess(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if (auth()->guard('user')->attempt($credentials)) {
            return redirect()->route('user.test.index');
        }

        return redirect()->back()->withErrors([
            'email' => 'Email atau password salah',
        ]);
    }

    public function logout()
    {
        auth()->guard('user')->logout();

        return redirect()->route('user.auth.login');
    }
}
