<?php

namespace App\Http\Controllers\User\Api;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Province;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    /** API City */
    public function apiCities(Request $request)
    {
        $cities = City::when($request->province, function ($query) use ($request) {
            $query->where('province_id', $request->province);
        })->get();

        return response()->json($cities, 200);
    }
}
