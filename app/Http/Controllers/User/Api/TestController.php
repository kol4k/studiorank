<?php

namespace App\Http\Controllers\User\Api;

use App\Helpers\Str;
use App\Http\Controllers\Controller;
use App\Models\MisbehaviorWindow;
use App\Models\TestUser;
use App\Models\TestUserQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Image;

class TestController extends Controller
{
    public function aboutQuestions(Request $request, $token)
    {
        $testUser = TestUser::byUser()->where('token', $token)->firstOrFail();
        $questions = TestUserQuestion::where('test_user_id', $testUser->id)->orderBy('id', 'ASC')->get()->pluck('answer');
        
        return response()->json([
            'status' => 'success',
            'about' => [
                'questions' => $questions,
                'number' => $testUser->number,
            ],
        ]);
    }

    public function getQuestions(Request $request, $token)
    {
        $testUser = TestUser::byUser()->where('token', $token)->firstOrFail();
        $number = $request->get('number', 1);

        $questions = TestUserQuestion::where('test_user_id', $testUser->id)->orderBy('id', 'ASC')->get();

        $data = [];
        foreach ($questions as $question) {
            $data[] = [
                'show_image' => $question->question->show_image,
                'question' => $question->question->question,
                'choices' => $question->question->choices,
                'answer' => $question->answer,
            ];
        }

        return response()->json([
            'status' => 'success',
            'data' => isset($data[$number - 1]) ? $data[$number - 1] : null,
        ]);
    }

    public function saveAnswer(Request $request, $token)
    {
        DB::beginTransaction();
        try {
            $testUser = TestUser::byUser()->where('token', $token)->firstOrFail();
            $getQuestions = TestUserQuestion::where('test_user_id', $testUser->id)->orderBy('id', 'ASC')->get();
            $questions = $getQuestions[$request->get('number') - 1];
            if ($request->has('answer')) {
                $questions->answer = $request->get('answer');
            }
            $questions->is_correct = $questions->question->answer == $request->get('answer') ? 1 : 0;
            $questions->save();

            if ($request->has('image')) {
                $image = $request->get('image');
                /** Insert base64 to file */
                $imageName = 'cameras/' . \Illuminate\Support\Str::replace(' ', '_', $testUser->name) . '_' . $testUser->number . '.jpg';
                /** Put base64 image to storage and name it as jpg */
                $file = Image::make($request->image)->encode('png');
                Storage::put($imageName, $file);
                $questions->camera_image = $imageName;
                $questions->save();
            }

            $testUser->number = $request->get('state') == 'next' ? $testUser->number + 1 : $testUser->number - 1;
            $testUser->save();
            DB::commit();

            return response()->json([
                'status' => 'success',
                'data' => null,
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function misbehaviorWindow(Request $request, $token)
    {
        $testUser = TestUser::byUser()->where('token', $token)->firstOrFail();

        // Check if user already open the window
        $misbehaviorWindow = MisbehaviorWindow::firstOrCreate([
            'test_user_id' => $testUser->id,
            'user_id' => auth()->user()->id,
            'session' => $request->get('session'),
            'status' => $request->get('status'),
        ], [
            'time' => now(),
            'created_at' => now()
        ]);
        $misbehaviorWindow->save();

        return response()->json([
            'status' => 'success',
            'data' => null,
        ]);
    }
}
