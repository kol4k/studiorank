/** Function Here */
function initTagify(targetHTML, configuration) {
    new Tagify(targetHTML, configuration);
}

function previewImage(srcComponent, targetComponent) {
    var target = $(targetComponent).attr('src', URL.createObjectURL(srcComponent.target.files[0]));
    target.onload = function() {
        URL.revokeObjectURL(output.src);
    }
}

async function initCamera(targetHTML) {
    const constraints = window.constraints = {
        audio: false,
        video: true
    };

    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.mediaDevices.getUserMedia;
    // Get the video element from the DOM and set the source to the webcam stream
    var camera = document.getElementById(targetHTML);

    if (navigator.getUserMedia) {
        var cameraStream = await navigator.mediaDevices.getUserMedia({
            video: true
        });
        
        camera.srcObject = cameraStream;
        camera.play();
    }
}

function formCity(srcComponent, targetComponent, selectedCity = null) {
    var valueSrc = $(srcComponent).val();
    var targetComponent = $(targetComponent);

    $.ajax({
        type: "GET",
        url: "/api/cities",
        data: {
            province: valueSrc
        },
        success: function (data) {
            targetComponent.html('');
            $.each(data, function (key, value) {
                targetComponent.append('<option value="' + value.id + '"' + (value.id == selectedCity ? 'selected' : '') + '>' + value.name + '</option>');
            });
        }
    });
}

$(document).ready(function() {
})