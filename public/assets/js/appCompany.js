/** Function Here */
function initTable(targetHTML) {
    var table = $(targetHTML).DataTable({"scrollX": true});
}

function initTagify(targetHTML, configuration) {
    new Tagify(targetHTML, configuration);
}

function initDaterangepicker(targetHTML, configuration) {
    $(targetHTML).daterangepicker(configuration);
}

function openModalDelete(targetURL) {
    $('#urlDelete').attr('href', targetURL);
    $('#modalDelete').modal('show');
}

$(document).ready(function() {
    /** Configration */
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toastr-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    
    /** Declare Here */
    initTable('.datatable');
    initDaterangepicker('.daterangepicker-between', {
        timePicker: true,
        timePicker24Hour: true,
        locale: {
            format: "DD/M/YYYY H:mm"
        }
    })
})