<x-mail::message>
This email address was recently entered to activate your StudioRank account.<br>
You can use this email and password to log in to StudioRank.<br>
You can use this information to verify that this email belongs to you.<br>
<br>
Email: {{ $user->email }}<br>
Password: {{ $password }}<br>
Link: <a href="https://app.studiorank.xyz/login">https://app.studiorank.xyz/login</a>
<br><br>
If this wasn't you, someone may have mistyped their email address. Keep this information to yourself, and no other action is needed at this moment.<br>
The StudioRank Accounts Team,<br>
{{ config('app.name') }}
</x-mail::message>
