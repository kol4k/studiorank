<x-mail::message>
This e-mail contains important information about the exam(s) you booked and the rules. Please ensure these details are correct. If any information is not correct, please contact StudioRank immediately.<br><br>
**Appointment Details**
<br>
Order: {{ $user->email }}
<br><br>
Exam: {{ $test->name }}<br>
Candidate: {{ $user->name }}<br>
Candidate Testing: {{ $user->email }}<br>
Registration ID: -<br>
Date: {{ \Carbon\Carbon::parse($test->start_test)->format('l, F d, Y') }}<br>
Time: {{ \Carbon\Carbon::parse($test->start_test)->format('g:i a') }} AM West Indonesia Time<br>
Appointment Length: {{ \Carbon\Carbon::parse($test->start_test)->diffInMinutes($test->end_test) }} Minutes<br>
System test: Test your system<br>
Exam check-in: Check in to start exam<br>
Check-in begins 30 minutes prior to the appointment time or, sign into your candidate website.<br>
<br>
<br>
**Admission Policy**
<br>
This exam is only proctored in English. There is not an option for local language proctoring at this time.<br>
<br>
**Before exam day:**
<br>
Test your system:<br>

*   Use the same testing space, computer, and internet connection you plan to use on exam day
*   We recommend testing on a personal computer with a webcam. Work computers generally have more restrictions that may prevent successful delivery
*   Do not take your exam in a setting with a corporate firewall (including VPN). If you are taking your online exam in your office, please alert your Network Administrator to the OnVUE system requirements
*   Make sure you have a reliable, fast internet connection to download your exam and support a webcam stream

Acquire an acceptable form of ID:<br>

One (1) valid, unexpired, government-issued ID with a signature and photo. The name must match the name on the registration exactly.<br>

Acceptable forms of ID include

*   Passport
*   Driver’s license
*   Non-US Military ID (including spouse and dependents)
*   Identification card (national or local)
*   Registration card (green card, permanent residence, visa)

Unacceptable forms of ID:

*   Unacceptable forms of identification include renewal forms with expired IDs, government-issued name change documents with government ID
*   If your identification is not considered valid, you will not be permitted to complete your exam and you are unlikely to receive a refund

By law, certain IDs must not be photocopied, digitized, or captured on camera. Because of this, any exam cannot accept restricted IDs (including but not limited to U.S. military or certain secure access IDs) as identification for online testing.

Prepare your testing space:

*   Find a quiet, disruption-free, well-lit space. Please be aware that background light or the sun rising/setting could create visibility issues for the proctor
*   You will be required to take four photos of your testing space during check-in. We recommend using a mobile device
*   You can only use one monitor
*   Your desk should be clear

For more [reference or information](https://home.pearsonvue.com/cppinstitute/onvue), review online proctored exam policies, procedures, and minimum requirements

**On exam day:**

*   We recommend logging into your account **30 minutes early** to start the check-in process and to allow for any troubleshooting
*   This allows optimal preparedness but does not guarantee an early admittance or start time
*   If you are more than 15 minutes late after your scheduled exam time, you will be unable to begin your exam and you are unlikely to receive a refund

To check into your exam:

*   Select Check-in to start the exam on your confirmation, reschedule or reminder emails
*   View your upcoming appointment
*   Follow the on-screen prompts to complete the check-in process
*   You will need to complete check-in before you can start your exam

Exam rules during testing

*   You may not access unauthorized materials, I.e. mobile devices, watches, and anything not allowed by test sponsor ·
*   No one else should appear through the webcam during the exam and no one else’s voice should be heard
*   No speaking aloud during exam

**Reschedule Policy**

If you need to reschedule your appointment, be sure to do it as soon as possible before your appointment. If you do not reschedule your appointment before the scheduled appointment time, you will forfeit your exam fee.

**Cancellation Policy**

If you need to cancel your appointment, be sure to do it as soon as possible before your appointment. If you do not cancel your appointment before the scheduled appointment time, you will forfeit your exam fee.

**Additional Information**

Please note that your “appointment length” may include time for non-scored sections such as a tutorial. The appointment length does not necessarily reflect the total amount of time allotted for the actual exam.

  
StudioRank will not be held responsible for expenses incurred beyond the cost of the exam, including but not limited to travel expenses and lost wages on the day of the exam.

Important: Please go to the Government or test center web page the day before your exam to review the latest face mask policy and other health and safety measures required at your test center.

StudioRank's goal is to make your exam experience a pleasant one. We want to thank you for selecting StudioRank as your exam service provider, and we look forward to serving you again. Please feel free to contact us with your comments or questions at hello@studiorank.xyz

Sincerely,

StudioRank
</x-mail::message>
