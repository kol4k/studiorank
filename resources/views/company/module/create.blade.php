@extends('layouts.appCompany')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="POST" action="{{ Route('company.module.store') }}">
            @csrf
            <div class="card card-flush mb-5 border">
                <div class="card-header">
                    <div class="card-title">
                        <h4>Tambah Modul</h4>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Nama Modul</label>
                        <input type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" name="name" placeholder="Nama Modul" value="{{ old('name') }}" autocomplete="off">
                        @error('name')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="form-label">Keterangan</label>
                        <textarea class="form-control form-control-sm @error('description') is-invalid @enderror" name="description" placeholder="Keterangan">{{ old('description') }}</textarea>
                        @error('description')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Tampilan Peringkat</label>
                        <select class="form-select form-select-sm @error('grade') is-invalid @enderror" name="grade" data-control="select2" data-hide-search="true" data-placeholder="Pilih Tampilan Peringkat">
                            <option value=""></option>
                            @foreach ($gradesGroup as $grade)
                            <option value="{{ $grade->id }}" {{ old('grade') == $grade->id ? 'selected' : '' }}>{{ $grade->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Status Menampilkan</label>
                        <select class="form-select form-select-sm @error('is_publish') is-invalid @enderror" name="is_publish" data-control="select2" data-hide-search="true" data-placeholder="Pilih Status Menampilkan">
                            <option value="" disabled selected></option>
                            <option value="1" {{ old('is_publish') == 1 ? 'selected' : '' }}>Tampilkan</option>
                            <option value="0" {{ old('is_publish') == 0 ? 'selected' : '' }}>Jangan Ditampilkan</option>
                        </select>
                        @error('is_publish')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mt-7">
                        <a href="{{ Route('company.module.index') }}" class="btn btn-sm btn-dark">
                            <i class="fa fa-arrow-left mr-2"></i>
                            Batalkan dan Kembali
                        </a>
                        <button type="submit" class="btn btn-primary btn-sm px-10">Simpan & Tambah Soal</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection