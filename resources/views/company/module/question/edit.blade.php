@extends('layouts.appCompany')

@section('script')
<script>
    $(document).ready(function () {
        /* Tagify */
        var config = {
            maxTags: 1,
            whitelist: [
                @foreach($categories as $category)
                "{{ $category->name }}",
                @endforeach
            ],
            dropdown: {
                maxItems: 20,
                classname: "",
                enabled: 0,
                closeOnSelect: false
            }
        }
        initTagify(document.getElementById('category'), config);

        /* Form Repeater */
        var alphabet = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
        var $repeaterAnswer = $('#answers').repeater({
            initEmpty: false,
            show: function() {
                setTimeout(() => {
                    initialize();
                    $(this).slideDown();
                }, 50);
            },
            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
                setTimeout(() => {
                    initialize();
                }, 50);
            }
        });
        function initialize() {
            $('.begin-answer').map(function(index, element) {
                $(this).find('.form-label').html('Opsi ' + alphabet[index]);
                $(this).find('textarea').attr('placeholder', 'Opsi ' + alphabet[index]);
                $(this).find('textarea').attr('data', index);
            });
        }
        $('.selectAnswer').click(function() {
            $('.selectAnswer').prop('checked', false);
            $(this).prop('checked', true);
        });
    });
</script>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="POST" action="{{ Route('company.module.question.update', $question->id) }}" enctype="multipart/form-data">
            @csrf
            <div class="card card-flush mb-5 border">
                <div class="card-header">
                    <div class="card-title">
                        <h4>Edit Soal</h4>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="mb-10 fv-row">
                        <label class="form-label">Keterangan</label>
                        <div>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe, culpa aspernatur veniam mollitia excepturi inventore quasi dolores? Laudantium temporibus officia facilis sint enim quibusdam fuga iusto fugiat id. Repellat, nemo.
                        </div>
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Kategori</label>
                        <input type="category" class="form-control form-control-sm @error('category') is-invalid @enderror" name="category" id="category" placeholder="Ketik baru nama kategori atau pilih yang sudah ada" value="{{ $question->category->name }}" autocomplete="off">
                        @error('category')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="form-label">Gambar Soal</label>
                        <input type="file" class="form-control form-control-sm @error('image') is-invalid @enderror" name="image" placeholder="Gambar Soal" value="{{ old('image') }}" accept="image/*" autocomplete="off">
                        @error('image')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Pertanyaan</label>
                        <textarea class="form-control @error('question') is-invalid @enderror" name="question" placeholder="Pertanyaan">{!! $question->question !!}</textarea>
                        @error('question')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div id="answers">
                        <div class="form-group">
                            <div data-repeater-list="answers">
                                @foreach(json_decode($question->choices) as $key => $choice)
                                <div data-repeater-item>
                                    <div class="begin-answer">
                                        <div class="mb-5 fv-row">
                                            <!-- index to alphabet -->
                                            <label class="required form-label">Opsi {{ $key }}</label>
                                            <textarea class="form-control" name="choice" placeholder="Opsi A" autocomplete="off">{{ $choice }}</textarea>
                                        </div>
                                        <div class="mb-5">
                                            <div class="d-flex justify-content-between">
                                                <div class="form-check form-check-custom form-check-solid">
                                                    <input class="form-check-input selectAnswer" type="checkbox" name="answer" value="1" {{ $question->answer == $key ? 'checked' : '' }}>
                                                    <label class="form-check-label">
                                                        Jawaban Benar
                                                    </label>
                                                </div>
                                                <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-light-danger mt-3">
                                                    <i class="la la-trash-o"></i>Hapus Row
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group mt-5">
                            <a href="javascript:;" data-repeater-create class="btn btn-light-dark btn-sm">
                                <i class="la la-plus"></i>Tambah Row
                            </a>
                        </div>
                    </div>
                    <div class="mt-7">
                        <a href="{{ Route('company.module.show', $question->module_id) }}" class="btn btn-sm btn-dark">
                            <i class="fa fa-arrow-left mr-2"></i>
                            Batalkan dan Kembali
                        </a>
                        <button type="submit" class="btn btn-primary btn-sm px-10">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection