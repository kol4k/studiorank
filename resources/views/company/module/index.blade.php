@extends('layouts.appCompany')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card border">
            <div class="card-header">
                <div class="card-title">
                    <h4>Daftar Modul</h4>
                </div>
                <div class="card-title">
                    <a href="{{ Route('company.module.create') }}" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i>
                        Tambah Modul
                    </a>
                </div>
            </div>
            <div class="card-body py-0">
                <table class="table align-middle table-row-dashed datatable gy-4">
                    <thead>
                        <tr class="fw-bold fs-6 text-gray-800">
                            <th class="min-w-150px">Tipe Modul</th>
                            <th class="min-w-150px">Nama Modul</th>
                            <th class="min-w-150px">Tampilan Peringkat</th>
                            <th class="min-w-200px">Status Menampilkan</th>
                            <th class="min-w-100px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($modules as $module)
                        <tr>
                            <td>
                                @if($module->type == 'test')
                                    Test Assessment
                                @elseif($module->type == 'psychotest')
                                    Psikotest
                                @endif
                            </td>
                            <td>{{ $module->name }}</td>
                            <td>{{ $module->groupGrade->name }}</td>
                            <td>
                                @if($module->is_publish)
                                    <span class="badge badge-success">Ditampilkan</span>
                                @else
                                    <span class="badge badge-danger">Tidak Ditampilkan</span>
                                @endif   
                            </td>
                            <td>
                                <div class="d-flex justify-content-end flex-shrink-0">
                                    <a href="{{ Route('company.module.show', $module->id) }}" class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2">
                                        <i class="bi bi-eye"></i> Lihat
                                    </a>
                                    <button class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2" onclick="openModalDelete('{{ Route('company.module.destroy', $module->id) }}')">
                                        <i class="bi bi-trash"></i> Hapus
                                    </button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection