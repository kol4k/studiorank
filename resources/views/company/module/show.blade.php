@extends('layouts.appCompany')

@section('content')
<div class="modal fade" tabindex="-1" id="editModul">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Edit Modul</h3>
            </div>
            <div class="modal-body">
                <form id="formUpdateModule" method="POST" action="{{ Route('company.module.update', $module->id) }}">
                    @csrf
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Nama Modul</label>
                        <input type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" name="name" placeholder="Nama Modul" value="{{ $module->name }}" autocomplete="off">
                        @error('name')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="form-label">Keterangan</label>
                        <textarea class="form-control form-control-sm @error('description') is-invalid @enderror" name="description" placeholder="Keterangan">{{ $module->description }}</textarea>
                        @error('description')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Tampilan Peringkat</label>
                        <select class="form-select form-select-sm @error('grade') is-invalid @enderror" name="grade" data-control="select2" data-hide-search="true" data-placeholder="Pilih Tampilan Peringkat">
                            <option value=""></option>
                            @foreach ($gradesGroup as $grade)
                            <option value="{{ $grade->id }}" {{ $module->grade_group_id == $grade->id ? 'selected' : '' }}>{{ $grade->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Status Menampilkan</label>
                        <select class="form-select form-select-sm @error('is_publish') is-invalid @enderror" name="is_publish" data-control="select2" data-hide-search="true" data-placeholder="Pilih Status Menampilkan">
                            <option value="" disabled selected></option>
                            <option value="1" {{ isset($module->is_publish) && $module->is_publish == 1 ? 'selected' : '' }}>Tampilkan</option>
                            <option value="0" {{ isset($module->is_publish) && $module->is_publish == 0 ? 'selected' : '' }}>Jangan Ditampilkan</option>
                        </select>
                        @error('is_publish')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light btn-sm" data-bs-dizsmiss="modal">Batalkan</button>
                <button type="submit" class="btn btn-primary btn-sm" form="formUpdateModule">Simpan Perubahan</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12 mb-7">
                <div class="card border">
                    <div class="card-header">
                        <div class="card-title">
                            <h4>Detail Modul</h4>
                        </div>
                        <div class="card-title">
                            <button class="btn btn-secondary btn-sm" data-bs-toggle="modal" data-bs-target="#editModul">
                                <i class="fa fa-pen"></i>
                                Edit Modul
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row mb-7">
                            <label class="col-lg-2 fw-semibold">Tipe Modul</label>
                            <div class="col-lg-8">
                                <span class="fw-bold text-gray-800">
                                    @if($module->type == 'test')
                                        Test Assessment
                                    @elseif($module->type == 'psychotest')
                                        Psikotest
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <label class="col-lg-2 fw-semibold">Nama Modul</label>
                            <div class="col-lg-8">
                                <span class="fw-bold text-gray-800">{{ $module->name }}</span>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <label class="col-lg-2 fw-semibold">Keterangan</label>
                            <div class="col-lg-8">
                                <span class="fw-bold fs-6 text-gray-800">{{ $module->description }}</span>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <label class="col-lg-2 fw-semibold">Tampilan Peringkat</label>
                            <div class="col-lg-8">
                                <span class="fw-bold fs-6 text-gray-800">{{ $module->groupGrade->name }}</span>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <label class="col-lg-2 fw-semibold">Status Menampilkan</label>
                            <div class="col-lg-8">
                                @if($module->is_publish)
                                    <span class="badge badge-success">Ditampilkan</span>
                                @else
                                    <span class="badge badge-danger">Tidak Ditampilkan</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 mb-7">
        <div class="card border">
            <div class="card-header">
                <div class="card-title">
                    <h4>Daftar Soal</h4>
                </div>
                <div class="card-title">
                    <a href="javascript::void();" class="btn btn-success btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="top-start">
                        <i class="fa fa-plus"></i>
                        Tambah Soal
                    </a>
                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-300px py-4" data-kt-menu="true">
                        <div class="menu-item px-3">
                            <a href="{{ Route('company.module.question.create', $module->id) }}" class="menu-link">
                                <i class="menu-icon fa-solid fa-plus"></i>
                                Tambah Satu Soal
                            </a>
                        </div>
                        <div class="menu-item px-3">
                            <a href="{{ Route('company.module.question.create-multiple', $module->id) }}" class="menu-link">
                                <i class="menu-icon fa-solid fa-upload"></i>
                                Tambah Banyak Soal
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body py-0">
                <table class="table align-middle table-row-dashed datatable gy-4">
                    <thead>
                        <tr class="fw-bold fs-6 text-gray-800">
                            <th class="min-w-50px">No</th>
                            <th class="min-w-150px">Gambar Soal</th>
                            <th class="min-w-400px">Soal</th>
                            <th class="min-w-600px">Pilihan Ganda</th>
                            <th class="min-w-50px">Jawaban</th>
                            <th class="min-w-200px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($module->questions->sortByDesc('created_at') as $question)
                        <tr>
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td class="text-center">
                                @if ($question->show_image)
                                <img src="{{ $question->show_image }}" class="img-fluid" width="55px">
                                @else
                                -
                                @endif
                            </td>
                            <td>{!! nl2br($question->question) !!}</td>
                            <td>
                                <ul type="A">
                                @foreach(json_decode($question->choices) as $choice)
                                    <li>{!! nl2br($choice) !!}</li>
                                @endforeach
                                </ul>
                            </td>
                            <td class="fw-bold text-center">{{ $question->answer }}</td>
                            <td>
                                <div class="d-flex justify-content-end flex-shrink-0">
                                    <a href="{{ Route('company.module.question.edit', $question->id) }}" class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2">
                                        <i class="bi bi-pencil"></i> Edit
                                    </a>
                                    <button class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2" onclick="openModalDelete('{{ Route('company.module.question.destroy', $question->id) }}')">
                                        <i class="bi bi-trash"></i> Hapus
                                    </button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection