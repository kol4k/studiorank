@extends('layouts.appCompany')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="POST" action="{{ Route('company.test.update', $test->id) }}">
            @csrf
            <div class="card card-flush mb-5 border">
                <div class="card-header">
                    <div class="card-title">
                        <h4>Ubah Tes</h4>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Nama Tes</label>
                        <input type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" name="name" placeholder="Nama Tes" value="{{ $test->name }}" autocomplete="off">
                        @error('name')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Deskripsi Tes</label>
                        <textarea class="form-control form-control-sm texteditor @error('description') is-invalid @enderror" name="description" placeholder="Deskripsi Tes">{{ $test->description }}</textarea>
                        @error('description')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Tanggal Pengerjaan</label>
                        <input type="text" class="form-control form-control-sm daterangepicker-between @error('date') is-invalid @enderror" name="date" placeholder="Waktu Pengerjaan" value="{{ $test->start_test->format('d/m/Y H:i') }} - {{ $test->end_test->format('d/m/Y H:i') }}" autocomplete="off">
                        @error('start_test')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        @error('end_test')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Status Tampilan</label>
                        <select class="form-select form-select-sm @error('status') is-invalid @enderror" name="status" data-control="select2" data-hide-search="true" data-placeholder="Pilih Status Tampilan">
                            <option value="" disabled selected></option>
                            <option value="1" {{ $test->status == 1 ? 'selected' : '' }}>Tampilkan</option>
                            <option value="0" {{ $test->status == 0 ? 'selected' : '' }}>Sembunyikan</option>
                        </select>
                    </div>
                    <div class="d-flex flex-column mb-5 fv-row">
                        <div class="fs-5 fw-bolder form-label">Pilih Modul</div>
                        @error('modules')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="row">
                        @foreach($modules as $module)
                        <div class="col-lg-3 mb-6">
                            <div class="card rounded-3 border border-dashed border-primary h-300px">
                                <div class="card-body">
                                    <label class="form-check form-check-custom form-check-solid pb-5">
                                        <input type="checkbox" class="form-check-input me-2" name="modules[]" value="{{ $module->id }}" {{ in_array($module->id, $test->modules->pluck('id')->toArray()) ? 'checked' : '' }}>
                                        <span class="fs-8 fw-bolder text-primary">{{ $module->name }}</span>
                                    </label>
                                    <span class="form-check-label text-gray-600 fs-7">{{ $module->description }}</span>
                                </div>
                                <div class="card-footer py-5">
                                    <a href="{{ Route('company.module.show', $module->id) }}" target="_blank" class="text-primary">
                                        <i class="far fa-arrow-alt-circle-down text-primary me-2"></i>Lihat Modul
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="d-flex flex-column mb-5 mt-5 fv-row">
                        <div class="fs-5 fw-bolder form-label">Pengaturan Mengerjakan Tes</div>
                    </div>
                    <div class="mb-10 fv-row">
                        <div class="form-check form-check-custom form-check-solid mb-2">
                            <input class="form-check-input @error('camera_supervision') is-invalid @enderror" type="checkbox" name="camera_supervision" value="1" {{ $test->camera_supervision ? 'checked' : '' }} />
                            <label class="form-check-label fw-bolder fs-7">Aktifkan Camera Supervision</label>
                        </div>
                        <div class="fs-7">Centang kotak ini untuk mengaktifkan fitur pengawasan kamera yang memungkinkan sistem menangkap gambar kandidat secara berkala selama pengujian. Fitur ini berfungsi paling baik dengan browser Google Chrome untuk PC, laptop, dan perangkat Android.</div>
                        @error('camera_supervision')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mt-7">
                        <a href="{{ Route('company.grade.index') }}" class="btn btn-sm btn-dark">
                            <span class="svg-icon svg-icon-5"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M9.60001 11H21C21.6 11 22 11.4 22 12C22 12.6 21.6 13 21 13H9.60001V11Z" fill="white" />
                                    <path opacity="0.3" d="M9.6 20V4L2.3 11.3C1.9 11.7 1.9 12.3 2.3 12.7L9.6 20Z" fill="white" />
                                </svg>
                            </span>
                            Batalkan dan Kembali
                        </a>
                        <button type="submit" class="btn btn-primary btn-sm px-10">Ubah Tes</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection