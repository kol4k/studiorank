@extends('layouts.appCompany')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="POST" action="{{ Route('company.test.store') }}">
            @csrf
            <div class="card card-flush mb-5 border">
                <div class="card-header">
                    <div class="card-title">
                        <h4>Tambah Tes</h4>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Nama Tes</label>
                        <input type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" name="name" placeholder="Nama Tes" value="{{ old('name') }}" autocomplete="off">
                        @error('name')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Deskripsi Tes</label>
                        <textarea class="form-control form-control-sm texteditor @error('description') is-invalid @enderror" name="description" placeholder="Deskripsi Tes">{{ old('description') }}</textarea>
                        @error('description')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Tanggal Pengerjaan</label>
                        <input type="text" class="form-control form-control-sm daterangepicker-between @error('date') is-invalid @enderror" name="date" placeholder="Waktu Pengerjaan" value="{{ old('date') }}" autocomplete="off">
                        @error('start_test')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        @error('end_test')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Status Tampilan</label>
                        <select class="form-select form-select-sm @error('status') is-invalid @enderror" name="status" data-control="select2" data-hide-search="true" data-placeholder="Pilih Status Tampilan">
                            <option value="" disabled selected></option>
                            <option value="1" {{ old('status') && old('status') == 1 ? 'selected' : '' }}>Tampilkan</option>
                            <option value="0" {{ old('status') && old('status') == 0 ? 'selected' : '' }}>Sembunyikan</option>
                        </select>
                    </div>
                    <div class="d-flex flex-column mb-5 fv-row">
                        <div class="fs-5 fw-bolder form-label">Pilih Modul</div>
                        @error('modules')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="row">
                        @foreach($modules as $module)
                        <div class="col-lg-3 mb-6">
                            <div class="card rounded-3 border border-dashed border-primary h-300px">
                                <div class="card-body">
                                    <label class="form-check form-check-custom form-check-solid pb-5">
                                        <input type="checkbox" class="form-check-input me-2" name="modules[]" value="{{ $module->id }}" {{ old('modules') && in_array($module->id, old('modules')) ? 'checked' : '' }}>
                                        <span class="fs-8 fw-bolder text-primary">{{ $module->name }}</span>
                                    </label>
                                    <span class="form-check-label text-gray-600 fs-7">{{ $module->description }}</span>
                                </div>
                                <div class="card-footer py-5">
                                    <a href="{{ Route('company.module.show', $module->id) }}" target="_blank" class="text-primary">
                                        <i class="far fa-arrow-alt-circle-down text-primary me-2"></i>Lihat Modul
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="d-flex flex-column mb-5 mt-5 fv-row">
                        <div class="fs-5 fw-bolder form-label">Pengaturan Mengerjakan Tes</div>
                    </div>
                    <div class="mb-10 fv-row">
                        <div class="form-check form-check-custom form-check-solid mb-2">
                            <input class="form-check-input @error('camera_supervision') is-invalid @enderror" type="checkbox" name="camera_supervision" value="1" />
                            <label class="form-check-label fw-bolder fs-7">Aktifkan Camera Supervision</label>
                        </div>
                        <div class="fs-7">Centang kotak ini untuk mengaktifkan fitur pengawasan kamera yang memungkinkan sistem menangkap gambar kandidat secara berkala selama pengujian. Fitur ini berfungsi paling baik dengan browser Google Chrome untuk PC, laptop, dan perangkat Android.</div>
                        @error('camera_supervision')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mt-7">
                        <a href="{{ Route('company.test.index') }}" class="btn btn-sm btn-dark">
                            <i class="fa fa-arrow-left mr-2"></i>
                            Batalkan dan Kembali
                        </a>
                        <button type="submit" class="btn btn-primary btn-sm px-10">Simpan & Tambah Soal</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection