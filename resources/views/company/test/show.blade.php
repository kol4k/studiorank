@extends('layouts.appCompany')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12 mb-7">
                <div class="card border">
                    <div class="card-header">
                        <div class="card-title">
                            <h4>Detail Tes</h4>
                        </div>
                        <div class="card-title">
                            <a href="{{ Route('company.test.edit', $test->id) }}" class="btn btn-secondary btn-sm">
                                <i class="fa fa-pen"></i>
                                Edit Tes
                            </a>
                        </div>
                    </div>
                    <div class="card-body pt-4">
                        <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x mb-5 fs-6 mt-0">
                            <li class="nav-item">
                                <a class="nav-link active" data-bs-toggle="tab" href="#umum">
                                    <span class="fw-bolder text-black">Umum</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#modul">
                                    <span class="fw-bolder text-black">Modul Pilihan</span>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="umum" role="tabpanel">
                                <div class="row mb-7">
                                    <label class="col-lg-2 fw-semibold">Nama Tes</label>
                                    <div class="col-lg-8">
                                        <span class="fw-bold text-gray-800">{{ $test->name }}</span>
                                    </div>
                                </div>
                                <div class="row mb-7">
                                    <label class="col-lg-2 fw-semibold">Deskripsi</label>
                                    <div class="col-lg-8">
                                        <span class="fw-bold text-gray-800">{{ $test->description }}</span>
                                    </div>
                                </div>
                                <div class="row mb-7">
                                    <label class="col-lg-2 fw-semibold">Jadwal Tes</label>
                                    <div class="col-lg-8">
                                        <span class="fw-bold text-gray-800">{{ $test->start_test }} - {{ $test->end_test }} ({{ \Carbon\Carbon::parse($test->start_test)->diffInMinutes($test->end_test) }} Menit)</span>
                                    </div>
                                </div>
                                <div class="row mb-7">
                                    <label class="col-lg-2 fw-semibold">Jumlah Modul</label>
                                    <div class="col-lg-8">
                                        <span class="fw-bold text-gray-800">{{ $test->modules->count() }}</span>&nbsp;&nbsp;&nbsp;<a href="#modul" class="text-primary">(Lihat Modul)</a>
                                    </div>
                                </div>
                                <div class="row mb-7">
                                    <label class="col-lg-2 fw-semibold">Status Tampilan</label>
                                    <div class="col-lg-8">
                                        <span class="fw-bold text-gray-800">{{ $test->status == 1 ? 'Ditampilkan' : 'Disembunyikan' }}</span>
                                    </div>
                                </div>
                                <div class="row mb-7">
                                    <label class="col-lg-2 fw-semibold">Supervisi Kamera</label>
                                    <div class="col-lg-8">
                                        <span class="fw-bold text-gray-800">{{ $test->camera_supervision ? 'Aktif' : 'Tidak Aktif' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="modul" role="tabpanel">
                                <ul>
                                    @foreach ($test->modules as $module)
                                        <li>{{ $module->name }} - {{ $module->questions->count() }} Soal&nbsp;&nbsp;&nbsp;<a href="{{ Route('company.module.show', $module->id) }}">(Lihat Modul)</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 mb-7">
        <div class="card border">
            <div class="card-header">
                <div class="card-title">
                    <h4>Daftar Peserta</h4>
                </div>
                <div class="card-title">
                    <div class="card-toolbar">
                        <a href="javascript::void();" class="btn btn-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="top-start">
                            <i class="fa fa-plus"></i>
                            Tambah Peserta
                        </a>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-300px py-4" data-kt-menu="true">
                            <div class="menu-item px-3">
                                <a href="{{ Route('company.test.invititation.create', $test->id) }}" class="menu-link">
                                    <i class="menu-icon fa-solid fa-plus"></i>
                                    Tambah Satu Peserta
                                </a>
                            </div>
                            <div class="menu-item px-3">
                                <a href="{{ Route('company.test.invititation.create-multiple', $test->id) }}" class="menu-link">
                                    <i class="menu-icon fa-solid fa-upload"></i>
                                    Tambah Banyak Peserta
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body py-0">
                <table class="table align-middle table-row-dashed datatable gy-4">
                    <thead>
                        <tr class="fw-bold fs-6 text-gray-800">
                            <th class="min-w-400px">Tanggal Diundang</th>
                            <th class="min-w-400px">Nama</th>
                            <th class="min-w-400px">Email</th>
                            <th class="min-w-200px">Status Pengerjaan</th>
                            <th class="min-w-200px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($test->participants as $participant)
                        <tr>
                            <td>{{ $participant->created_at }}</td>
                            <td>{{ $participant->name }}</td>
                            <td>{{ $participant->email }}</td>
                            <td>
                                @if($participant->pivot->status == 1)
                                    <span class="badge badge-secondary">Belum Mengerjakan</span>
                                @elseif($participant->pivot->status == 2)
                                    <span class="badge badge-info">Sedang Mengerjakan</span>
                                @elseif($participant->pivot->status == 3)
                                    <span class="badge badge-success">Selesai Mengerjakan</span>
                                @endif
                            </td>
                            <td>
                                @if($participant->pivot->status != 3)
                                <a href="#" class="btn btn-sm btn-light-secondary">Laporan Belum Ada</a>
                                @else
                                <a href="{{ Route('company.download', ['path' => $participant->pivot->file_report]) }}" class="btn btn-sm btn-light-primary">Lihat Laporan</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection