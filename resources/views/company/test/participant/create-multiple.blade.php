
@extends('layouts.appCompany')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="POST" action="{{ Route('company.test.invititation.store-multiple', $test->id) }}" enctype="multipart/form-data">
            @csrf
            @if(session('error'))
                @foreach(session('error') as $error)
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            <div class="card card-flush mb-5 border">
                <div class="card-header">
                    <div class="card-title">
                        <h4>Tambah Peserta</h4>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Berkas</label>
                        <input type="file" class="form-control form-control-sm @error('file') is-invalid @enderror" name="file" placeholder="Nama Peserta" value="{{ old('file') }}" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        @error('file')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mt-7">
                        <a href="{{ Route('company.test.show', $test->id) }}" class="btn btn-sm btn-dark">
                            <i class="fa fa-arrow-left mr-2"></i>
                            Batalkan dan Kembali
                        </a>
                        <button type="submit" class="btn btn-primary btn-sm px-10">Tambahkan Peserta</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection