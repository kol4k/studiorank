@extends('layouts.appCompany')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card border">
            <div class="card-header">
                <div class="card-title">
                    <h4>Daftar Tes</h4>
                </div>
                <div class="card-title">
                    <a href="{{ Route('company.test.create') }}" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i>
                        Tambah Tes
                    </a>
                </div>
            </div>
            <div class="card-body py-0">
                <table class="table align-middle table-row-dashed datatable gy-4 scrollable">
                    <thead>
                        <tr class="fw-bold fs-6 text-gray-800">
                            <th class="min-w-250px">Nama Tes</th>
                            <th class="min-w-200px">Jadwal Tes</th>
                            <th class="min-w-200px">Status Peserta</th>
                            <th class="min-w-150px">Status Tampilan</th>
                            <th class="min-w-100px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tests as $test)
                        <tr>
                            <td>{{ $test->name }}</td>
                            <td>
                                {{ $test->start_test }} - {{ $test->end_test }} ({{ \Carbon\Carbon::parse($test->start_test)->diffInMinutes($test->end_test) }} Menit)
                            </td>
                            <td>-</td>
                            <td>
                                @if($test->status == 1)
                                    <span class="badge badge-success">Ditampilkan</span>
                                @elseif ($test->status == 0)
                                    <span class="badge badge-secondary">Disembunyikan</span>
                                @endif
                            <td>
                                <div class="d-flex justify-content-end flex-shrink-0">
                                    <a href="{{ Route('company.test.show', $test->id) }}" class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2">
                                        <i class="bi bi-eye"></i> Lihat Detail
                                    </a>
                                </div>
                            </td>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection