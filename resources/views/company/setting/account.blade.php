@extends('layouts.appCompany')

@section('content')
<div class="row">
    <div class="col-lg-6">
        <form method="POST" action="{{ Route('company.setting.account-update') }}">
            @csrf
            <div class="card card-flush mb-5 border">
                <div class="card-header">
                    <div class="card-title">
                        <h4>Ubah Profil</h4>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Nama Akun</label>
                        <input type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" name="name" placeholder="Nama Tes" value="{{ auth()->guard('company')->user()->name }}" autocomplete="off">
                        @error('name')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mt-7">
                        <button type="submit" class="btn btn-primary btn-sm px-10">Simpan Perubahan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-lg-6">
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <form method="POST" action="{{ Route('company.setting.password-update') }}">
            @csrf
            <div class="card card-flush mb-5 border">
                <div class="card-header">
                    <div class="card-title">
                        <h4>Ubah Password</h4>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Password Lama</label>
                        <input type="password" class="form-control form-control-sm @error('old_password') is-invalid @enderror" name="old_password" placeholder="Password Lama" autocomplete="off">
                        @error('old_password')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Password Baru</label>
                        <input type="password" class="form-control form-control-sm @error('new_password') is-invalid @enderror" name="new_password" placeholder="Password Baru" autocomplete="off">
                        @error('new_password')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Konfirmasi Password Baru</label>
                        <input type="password" class="form-control form-control-sm @error('new_password_confirmation') is-invalid @enderror" name="new_password_confirmation" placeholder="Konfirmasi Password Baru" autocomplete="off">
                        @error('new_password_confirmation ')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mt-7">
                        <button type="submit" class="btn btn-primary btn-sm px-10">Ubah Kata Sandi</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection