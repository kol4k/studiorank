@extends('layouts.appCompany')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card border">
            <div class="card-header">
                <div class="card-title">
                    <h4>Daftar Ranking</h4>
                </div>
                <div class="card-title">
                    <a href="{{ Route('company.grade.create') }}" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i>
                        Tambah Ranking
                    </a>
                </div>
            </div>
            <div class="card-body py-0">
                <table class="table align-middle table-row-dashed datatable gy-4">
                    <thead>
                        <tr class="fw-bold fs-6 text-gray-800">
                            <th class="min-w-150px">Group</th>
                            <th class="min-w-200px">Tampilan Peringkat</th>
                            <th class="min-w-200px">Nilai</th>
                            <th class="min-w-100px">Warna</th>
                            <th class="min-w-100px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($grades as $grade)
                        <tr>
                            <td>{{ $grade->group->name }}</td>
                            <td>{{ $grade->name }}</td>
                            <td>{{ $grade->value_start }} - {{ $grade->value_end }}</td>
                            <td>
                                @if($grade->color == 'green')
                                <div class="badge text-dark" style="background-color: #00FF00;">
                                    <strong>HIJAU</strong>
                                </div>
                                @elseif($grade->color == 'yellow')
                                <div class="badge text-dark" style="background-color: #FFFF04;">
                                    <strong>KUNING</strong>
                                </div>
                                @elseif($grade->color == 'orange')
                                <div class="badge" style="background-color: #FF9902;">
                                    <strong>ORANYE</strong>
                                </div>
                                @elseif($grade->color == 'red')
                                <div class="badge" style="background-color: #FF0000;">
                                    <strong>MERAH</strong>
                                </div>
                                @elseif($grade->color == 'blue')
                                <div class="badge" style="background-color: #1255CC;">
                                    <strong>BIRU</strong>
                                </div>
                                @endif
                            </td>
                            <td>
                                <div class="d-flex justify-content-end flex-shrink-0">
                                    <a href="{{ Route('company.grade.edit', $grade->id) }}" class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2">
                                        <i class="bi bi-pencil"></i> Edit
                                    </a>
                                    <button class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2" onclick="openModalDelete('{{ Route('company.grade.destroy', $grade->id) }}')">
                                        <i class="bi bi-trash"></i> Hapus
                                    </button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection