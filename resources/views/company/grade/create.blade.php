@extends('layouts.appCompany')

@section('script')
<script>
    var config = {
        maxTags: 1,
        whitelist: [
            @foreach($groups as $group)
            "{{ $group->name }}",
            @endforeach
        ],
        dropdown: {
            maxItems: 20,
            classname: "",
            enabled: 0,
            closeOnSelect: false
        }
    }
    initTagify(document.getElementById('group'), config);
</script>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="POST" action="{{ Route('company.grade.store') }}">
            @csrf
            <div class="card card-flush mb-5 border">
                <div class="card-header">
                    <div class="card-title">
                        <h4>Tambah Peringkat</h4>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Pilih Group</label>
                        <input class="form-control form-control-sm @error('group') is-invalid @enderror" name="group" id="group" value="{{ old('group') }}" placeholder="Ketik baru nama group atau pilih yang sudah ada">
                        @error('group')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Tampilan Peringkat</label>
                        <input type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" name="name" placeholder="Tampilan Peringkat" value="{{ old('name') }}" autocomplete="off">
                        @error('name')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="required form-label">Nilai</label>
                        <div class="input-group input-group-sm mb-5">
                            <input type="text" class="form-control @error('value_start') is-invalid @enderror" name="value_start" placeholder="Nilai Awal" value="{{ old('value_start') }}">
                            <span class="input-group-text">-</span>
                            <input type="text" class="form-control @error('value_end') is-invalid @enderror" name="value_end" placeholder="Nilai Akhir" value="{{ old('value_end') }}">
                            @if($errors->has('value_start') || $errors->has('value_end'))
                            <div class="invalid-feedback">{{ $errors->first('value_start') }}</div>
                            <div class="invalid-feedback">{{ $errors->first('value_end') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="form-label">Hasil Peringkat</label>
                        <textarea class="form-control form-control-sm @error('result') is-invalid @enderror" name="result" placeholder="Hasil Peringkat">{{ old('result') }}</textarea>
                        @error('result')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-10 fv-row">
                        <label class="form-label">Warna Peringkat</label>
                        <select class="form-select form-select-sm @error('color') is-invalid @enderror" name="color" data-control="select2" data-hide-search="true" data-placeholder="Pilih Warna Peringkat">
                            <option value="" disabled selected>Merah</option>
                            <option value="red">Merah</option>
                            <option value="yellow">Kuning</option>
                            <option value="green">Hijau</option>
                            <option value="orange">Oranye</option>
                            <option value="blue">Biru</option>
                        </select>
                        @error('color')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mt-7">
                        <a href="{{ Route('company.grade.index') }}" class="btn btn-sm btn-dark">
                            <i class="fa fa-arrow-left mr-2"></i>
                            Batalkan dan Kembali
                        </a>
                        <button type="submit" class="btn btn-primary btn-sm px-10">Buat Peringkat</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection