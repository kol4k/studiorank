@extends('layouts.appUser')

@section('content')
<div class="row">
    <div class="col-lg-12">
        @if(Auth::guard('user')->user()->detail()->exists())
        <div class="card bg-primary mb-5">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="flex-grow-1 bgi-no-repeat bgi-size-contain bgi-position-x-center card-rounded-bottom h-200px mh-200px my-5 mb-lg-12" style="background-image:url('/assets/media/svg/illustrations/easy/5.svg')"></div>
                    </div>
                    <div class="col-lg-9 align-items-center">
                        <h1 class="fw-boldest text-white text-center lh-lg mb-5">Welcome!
                            <br />
                            <span class="fw-bold">Your profile has been completed. Now, you can start the test.</span>
                        </h1>
                        <!-- <div class="text-center">
                            <a href="javascript::void();" class="btn btn-sm bg-white">Masih Kebingungan? <span class="text-info">Download Panduan Pengerjaan Tes Disini</span></a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="card card-flush">
            <div class="card-header mt-5">
                <div class="card-title flex-column">
                    <h3 class="fw-bolder mb-1">List Test</h3>
                </div>
            </div>
            <div class="card-body pt-0">
                <div class="table-responsive">
                    <table id="kt_profile_overview_table" class="table table-row-bordered table-row-dashed gy-4 align-middle fw-bolder">
                        <thead class="fs-7 text-gray-400 text-uppercase">
                            <tr>
                                <th class="min-w-500px">Name</th>
                                <th class="min-w-450px">Date</th>
                                <th class="min-w-150px">Duration</th>
                                <th class="min-w-150px">Number of Questions</th>
                                <th class="min-w-200px">Status</th>
                                <th class="min-w-50px text-end">Detail</th>
                            </tr>
                        </thead>
                        <tbody class="fs-6">
                            @foreach($testsUser as $testUser)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="me-5 position-relative">
                                            <div class="symbol symbol-35px symbol-circle">
                                                <span class="symbol-label bg-light-warning text-warning fw-bold">{{ $loop->iteration }}</span>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-column justify-content-center">
                                            <a href="{{ Route('user.test.detail', $testUser->token) }}" class="fs-6 text-gray-800 text-hover-primary">{{ $testUser->test->name }}</a>
                                            <div class="fs-8 text-muted">{{ $testUser->test->company->name }}</div>
                                        </div>
                                    </div>
                                </td>
                                <td>{{ $testUser->test->start_test->format('d F Y H:i').' - '.$testUser->test->end_test->format('d F Y H:i') }}</td>
                                <td>{{ \Carbon\Carbon::parse($testUser->test->start_test)->diffInMinutes($testUser->test->end_test) }} Minutes</td>
                                <td>{{ $testUser->test->count_question }} Questions</td>
                                <td>
                                    @if($testUser->status == 1)
                                    <span class="badge badge-light-warning fw-bolder px-4 py-3">Not Started</span>
                                    @elseif($testUser->status == 2)
                                    <span class="badge badge-light-info fw-bolder px-4 py-3">Progress</span>
                                    @elseif($testUser->status == 3)
                                    <span class="badge badge-light-success fw-bolder px-4 py-3">Done</span>
                                    @endif
                                </td>
                                <td class="text-end">
                                    <a href="{{ Route('user.test.detail', $testUser->token) }}" class="btn btn-light btn-sm">View</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection