@extends('layouts.appUser')

@section('script')
<script>
    $(document).ready(function() {
        document.getElementById('checkCamera').addEventListener('click', function () {
            let camera = new Promise((resolve, reject) => {
                resolve(initCamera('camera'))
            });
            camera.then(function () {
                setTimeout(function () {
                    if (document.getElementById('camera').paused) {
                        document.getElementById('iconCamera').style.display = 'block';
                    } else {
                        document.getElementById('iconCamera').style.display = 'none';
                        document.getElementById('tnc').disabled = false;
                    }
                }, 200);
            });
        })

        document.getElementById('tnc').addEventListener('click', function () {
            if (document.getElementById('tnc').checked) {
                $('#modalTNC').modal('show');
            } else {
                document.getElementById('startTest').disabled = true;
            }
        })

        document.getElementById('acceptTNC').addEventListener('click', function () {
            if (!document.getElementById('camera').paused && document.getElementById('tnc').checked) {
                $('#modalTNC').modal('hide');
                document.getElementById('startTest').disabled = false;
            } else {
                document.getElementById('startTest').disabled = true;
            }
        })
    });
</script>
@endsection

@section('content')
<div class="row min-h-300px">
    <div class="col-lg-9">
        <div class="card h-100">
            <div class="card-header border-0">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bolder text-dark">Preparation</span>
                </h3>
            </div>
            <div class="card-body pt-0">
                <p>Please prepare your camera.</p>
                <div class="row mt-20 mb-20">
                    <div class="col-lg-12 d-flex flex-column align-items-center">
                        <div class="fw-bolder mb-4">Camera</div>
                        <div class="d-flex flex-center w-100px h-100px rounded-3 bg-light-primary bg-opacity-90 mb-4">
                            <video playsinline autoplay id="camera" class="w-100 p-3 rounded-3" data-camera="0" style="margin-left: 0px;"></video>
                            <i class="fas fa-camera fs-2x text-primary" id="iconCamera" style="margin-left: -100px;"></i>
                        </div>
                        <button class="btn btn-primary btn-sm" id="checkCamera">Camera Check</button>
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <div class="text-center">
                        <div class="form-check form-check-custom form-check-solid mb-5">
                            <input class="form-check-input @error('tnc') is-invalid @enderror" type="checkbox" name="tnc" id="tnc" value="1" disabled>
                            <label class="form-check-label" for="tnc">
                            I certify that I am ready to take the test and has read, understand, and agree about the terms of the test
                            </label>
                        </div>
                        <form method="POST" action="{{ Route('user.test.start-process', $testUser->token) }}">
                            @csrf
                            <button type="submit" class="btn btn-secondary btn-sm btn-block" id="startTest" disabled>
                                Start
                                <i class="fas fa-long-arrow-alt-right ml-1"></i>
                            </button>
                        </form>
                    </div>
                </div>
                <div class="modal fade" tabindex="-1" id="modalTNC">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Terms & Conditions</h5>
                                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                                    <span class="svg-icon svg-icon-2x"></span>
                                </div>
                            </div>
                            <div class="modal-body">
                                Terms & Conditions of <strong>{{ $testUser->test->name }}</strong> by {{ $testUser->test->company->name }} is accessible on <a href="https://docs.google.com/document/d/1xAUTuRVPlp0GMB3DAHG-2KWC4K0ymcKLME4RXN5VJZ4/edit" target="_blank" class="text-info">our document</a>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light btn-sm" data-bs-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary btn-sm" id="acceptTNC" data-bs-dismiss="modal">I agree</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card h-100">
            <div class="card-header border-0">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bolder text-dark">Rincian</span>
                </h3>
            </div>
            <div class="card-body pt-0">
                <div class="col-lg-12 mb-5">
                    <h5 class="fs-7 mb-2">Name</h5>
                    <div class="mb-0">
                        <span class="fs-7 fw-bold text-gray-600">{{ $testUser->test->name }}</span>
                    </div>
                </div>
                <div class="col-lg-12 mb-5">
                    <h5 class="fs-7 mb-2">Company / University</h5>
                    <div class="mb-0">
                        <span class="fs-7 fw-bold text-gray-600">{{ $testUser->test->company->name }}</span>
                    </div>
                </div>
                <div class="col-lg-12 mb-5">
                    <h5 class="fs-7 mb-2">Date</h5>
                    <div class="mb-0">
                        <span class="fs-7 fw-bold text-gray-600">{!! $testUser->test->start_test->format('d F Y H:i').' - <br>'.$testUser->test->end_test->format('d F Y H:i') !!}</span>
                    </div>
                </div>
                <div class="col-lg-12 mb-5">
                    <h5 class="fs-7 mb-2">Duration</h5>
                    <div class="mb-0">
                        <span class="fs-7 fw-bold text-gray-600">{{ \Carbon\Carbon::parse($testUser->test->start_test)->diffInMinutes($testUser->test->end_test) }} Minutes</span>
                    </div>
                </div>
                <div class="col-lg-12 mb-5">
                    <h5 class="fs-7 mb-2">Number of Questions</h5>
                    <div class="mb-0">
                        <span class="fs-7 fw-bold text-gray-600">{{ $testUser->test->count_question }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection