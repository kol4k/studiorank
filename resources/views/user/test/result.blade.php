@extends('layouts.appBase')

@section('script')
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card w-100 bg-light-primary">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <img src="{{ asset('assets/images/u-login.png') }}" alt="logo" class="img-fluid" style="max-width: 500px;">
                    </div>
                    <div class="col-lg-6 bg-white rounded-1 p-5 px-10">
                        <h1 class="text-center">Thank you!</h1>
                        <p class="text-center">You have finished the test and here is your summary:</p>
                        <table class="table table-border" style="margin-top: 50px; margin-bottom: 50px;">
                            <tr>
                                <td>Name</td>
                                <td>:</td>
                                <td>{{ $testUser->user->name }}</td>
                            </tr>
                            <tr>
                                <td>Name of Test</td>
                                <td>:</td>
                                <td>{{ $testUser->test->name }}</td>
                            </tr>
                            <tr>
                                <td>Start Time</td>
                                <td>:</td>
                                <td>{{ $testUser->start_test }}</td>
                            </tr>
                            <tr>
                                <td>End Time</td>
                                <td>:</td>
                                <td>{{ $testUser->end_test }}</td>
                            </tr>
                        </table>
                        <div class="text-center">
                            <a href="{{ route('user.test.index') }}" class="btn btn-primary btn-sm">Go to Dashboard</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection