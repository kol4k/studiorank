@extends('layouts.appUser')

@section('content')
<div class="modal fade" tabindex="-1" id="confirmStart">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="alert alert-dismissible d-flex flex-center flex-column py-10 px-10 px-lg-20 w-100">
                <button type="button" class="position-absolute top-0 end-0 m-2 btn btn-icon btn-icon-primary" data-bs-dismiss="modal" aria-label="Close">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="red" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="red" />
                        </svg>
                    </span>
                </button>
                <span class="svg-icon svg-icon-5tx svg-icon-primary mb-5">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="red" />
                        <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="red" />
                        <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="red" />
                    </svg>
                </span>
                <div class="text-center text-dark">
                    <h1 class="fw-bolder mb-5">Confirmation</h1>
                    <div class="separator separator-dashed border-primary opacity-25 mb-5"></div>
                    <div class="mb-9">
                        Are you sure want to start the test?<br>
                        <strong>You only have 1 chance to submit the test, ensure you are prepared.</strong>
                    </div>
                    <div class="d-flex flex-center flex-wrap">
                        <a href="javascript::void();" class="btn btn-secondary m-2" data-bs-dismiss="modal" aria-label="Close">Back</a>
                        <a href="{{ Route('user.test.start', $testUser->token) }}" class="btn btn-primary m-2">Start</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row min-h-300px">
    <div class="col-lg-8">
        <div class="card h-100">
            <div class="card-header border-0">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bolder text-dark">Description</span>
                </h3>
            </div>
            <div class="card-body pt-0">
                {!! $testUser->test->description !!}
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card" style="position: sticky; top: 110px; bottom: 40px;">
            <div class="card-header border-0">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bolder text-dark">Details</span>
                </h3>
            </div>
            <div class="card-body pt-0">
                <div class="col-lg-12 mb-5">
                    <h5 class="fs-7 mb-2">Name</h5>
                    <div class="mb-0">
                        <span class="fs-7 fw-bold text-gray-600">{{ $testUser->test->name }}</span>
                    </div>
                </div>
                <div class="col-lg-12 mb-5">
                    <h5 class="fs-7 mb-2">Company / University</h5>
                    <div class="mb-0">
                        <span class="fs-7 fw-bold text-gray-600">{{ $testUser->test->company->name }}</span>
                    </div>
                </div>
                <div class="col-lg-12 mb-5">
                    <h5 class="fs-7 mb-2">Date</h5>
                    <div class="mb-0">
                        <span class="fs-7 fw-bold text-gray-600">{!! $testUser->test->start_test->format('d F Y H:i').' - <br>'.$testUser->test->end_test->format('d F Y H:i') !!}</span>
                    </div>
                </div>
                <div class="col-lg-12 mb-5">
                    <h5 class="fs-7 mb-2">Duration</h5>
                    <div class="mb-0">
                        <span class="fs-7 fw-bold text-gray-600">{{ \Carbon\Carbon::parse($testUser->test->start_test)->diffInMinutes($testUser->test->end_test) }} Minutes</span>
                    </div>
                </div>
                <div class="col-lg-12 mb-5">
                    <h5 class="fs-7 mb-2">Number of Questions</h5>
                    <div class="mb-0">
                        <span class="fs-7 fw-bold text-gray-600">{{ $testUser->test->count_question }}</span>
                    </div>
                </div>
                <div class="col-lg-12 pt-5">
                    @if ($testUser->status == 1)
                    <button class="btn btn-primary btn-sm w-100" data-bs-toggle="modal" data-bs-target="#confirmStart">Start</button>
                    @elseif ($testUser->status == 2)
                    <a href="{{ Route('user.test.start', $testUser->token) }}" class="btn btn-primary btn-sm w-100">Continue</a>
                    @elseif ($testUser->status == 3)
                        @if ($testUser->file_report)
                        <a href="{{ Route('user.download', ['path' => $testUser->file_report]) }}" class="btn btn-primary btn-sm w-100">Download Report</a>
                        @else
                        <button type="button" class="btn btn-primary btn-sm w-100">Report in progress</button>
                        @endif
                    @endif
                    <a href="{{ route('user.test.index') }}" class="btn btn-secondary btn-sm w-100 mt-2">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection