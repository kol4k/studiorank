@extends('layouts.appBase')

@section('script')
<script>
/** Function for get about data */
function fetchAbout() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "{{ route('user.test.about.get', $testUser->token) }}",
            type: "GET",
            dataType: "json",
            success: function (response) {
                resolve(response);
            },
            error: function (error) {
                reject(error);
            }
        });
    });
}

/** Function for get data question */
function fetchQuestions(number) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "{{ route('user.test.question.get', $testUser->token) }}",
            type: "GET",
            data: {
                number: number
            },
            success: function (response) {
                resolve(response);
            },
            error: function (error) {
                reject(error);
            }
        });
    });
}

/** Function for save misbehavior window */
function fetchMisbehaviorWindow() {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "{{ route('user.test.question.misbehavior-window', $testUser->token) }}",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                resolve(response);
            },
            error: function (error) {
                reject(error);
            }
        });
    });
}

/** Function for save answer question */
function saveAnswer(number, answer, state) {
    return new Promise((resolve, reject) => {
        /** capture camera in id camera */
        let camera = document.getElementById("camera");
        let canvas = document.createElement("canvas");
        canvas.width = camera.videoWidth;
        canvas.height = camera.videoHeight;
        canvas.getContext('2d').drawImage(camera, 0, 0);
        let snap = canvas.toDataURL('image/jpeg');
        
        $.ajax({
            url: "{{ route('user.test.question.answer', $testUser->token) }}",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                number: number,
                answer: answer,
                state: state,
                image: snap
            },
            success: function (response) {
                resolve(response);
            },
            error: function (error) {
                reject(error);
            }
        });
    });
}

/** Function for save misbehavior window */
function saveMisbehaviorWindow(session, status) {
    return new Promise((resolve, reject) => {
        
        $.ajax({
            url: "{{ route('user.test.question.misbehavior-window', $testUser->token) }}",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                session: session,
                status: status
            },
            success: function (response) {
                resolve(response);
            },
            error: function (error) {
                reject(error);
            }
        });
    });
}

/** Function for parse about */
function parseHtmlFetchAbout() {
    let about = fetchAbout();
    about.then(function (response) {
        let questions = response.about.questions;
        let number = response.about.number;
        let question = questions[number - 1];

        // Foreach question
        let iterationHtml = '';
        questions.forEach(function (question, index) {
            let questionNumber = index + 1;
            let questionStatus = question ? 'answered' : 'unanswered';

            // Append question
            if (questionNumber == number) {
                iterationHtml += '<a href="javascript::;" class="btn btn-icon btn-warning me-2 mb-2">' + questionNumber + '</a>';
            } else if (question) {
                iterationHtml += '<a href="javascript::;" class="btn btn-icon btn-primary me-2 mb-2">' + questionNumber + '</a>';
            } else {
                iterationHtml += '<a href="javascript::;" class="btn btn-icon btn-light me-2 mb-2">' + questionNumber + '</a>';
            }
        });

        // Append question
        document.getElementsByClassName('titleIterationQuestion')[0].innerHTML = 'Question ' + number + ' of ' + questions.length;
        document.getElementsByClassName('titleNoQuestion')[0].innerHTML = 'Question ' + number;
        document.getElementsByClassName('iterationQuestion')[0].innerHTML = iterationHtml;

        // Replace question
        totalQuestion = questions.length;
    });
}

/** Function for parse question */
function parseHtmlFetchQuestions(number) {
    let question = fetchQuestions(number);
    question.then(function (response) {
        if (response.data) {
            /** Show question to class question */
            questionHtml = '';
            if (response.data.show_image) {
                questionHtml += '<div>';
                questionHtml += '<img src="' + response.data.show_image + '" class="img-fluid mb-5" alt="image" style="width: fit-content;">';
                questionHtml += '</div>';
            }
            if (response.data.question) {
                questionHtml += response.data.question.replace(/(\r\n|\r|\n)/g, '<br>');
            }
            /** Show choices to class choices */
            let choices = JSON.parse(response.data.choices);
            
            let choicesHtml = '';
            for (choice in choices) {
                choicesHtml += '<label class="d-flex flex-stack mb-5 fv-row rounded-3 p-7 btn btn-outline btn-outline-primary btn-outline-dashed hoverable">';
                choicesHtml += '<label class="form-check form-check-custom form-check-solid">';
                choicesHtml += '<input type="radio" class="form-check-input selectAnswer text-left" name="answer" value="' + choice + '"' + (response.data.answer == choice ? 'checked' : '') + '>'
                choicesHtml += '<span class="form-check-label fs-5 fw-bolder">' + choices[choice].replace(/(\r\n|\r|\n)/g, '<br>') + '</span>';
                choicesHtml += '</label>';
                choicesHtml += '</label>';
            }
            document.getElementsByClassName('question')[0].innerHTML = questionHtml;
            document.getElementsByClassName('contentAnswer')[0].innerHTML = choicesHtml;
            document.getElementsByClassName('preloader')[0].style.display = 'none';
        } else {
            document.getElementById('question').innerHTML = 'Test telah selesai';
            document.getElementById('choices').innerHTML = '';
        }
    });
}

let guid = () => {
    let s4 = () => {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

$(document).ready(function() {
    let totalQuestion = {{ $testUser->questions->count() }};
    var currentQuestion = {{ $testUser->number }};

    /** Hidden all */
    document.getElementById('startQuestion').style.display = 'none';
    document.getElementById('turnOnCamera').style.display = 'none';

    if (document.getElementById('camera').paused) {
        document.getElementById('turnOnCamera').style.display = 'block';
    } else {
        document.getElementById('turnOnCamera').style.display = 'none';
    }

    /** Check Camera */
    document.getElementById('checkCamera').addEventListener('click', function () {
        let camera = new Promise((resolve, reject) => {
            resolve(initCamera('camera'))
        });
        camera.then(function () {
            setTimeout(function () {
                if (document.getElementById('camera').paused) {
                    document.getElementById('startQuestion').style.display = 'none';
                    document.getElementById('turnOnCamera').style.display = 'block';
                } else {
                    document.getElementById('startQuestion').style.display = 'block';
                    document.getElementById('turnOnCamera').style.display = 'none';
                }
            }, 200);
        });
    });

    /** detect blur and focus window in one session and show alert */
    var nextUniqueWindow = null;
    var currentUniqueWindow = guid();
    window.addEventListener('blur', sendBlurWindow);
    window.addEventListener('focus', sendFocusWindow);
    
    function sendBlurWindow() {
        if (nextUniqueWindow == null || nextUniqueWindow == currentUniqueWindow) {
            nextUniqueWindow = guid();
        }
        let saveMisbehavior = saveMisbehaviorWindow(nextUniqueWindow, 'blur');
        saveMisbehavior.then(function (response) {
            $('#cheatingAlert').modal('show');
        });
    }
    
    function sendFocusWindow() {
        if (nextUniqueWindow != null && nextUniqueWindow != currentUniqueWindow) {
            currentUniqueWindow = nextUniqueWindow;
            nextUniqueWindow = null;
        }
        let saveMisbehavior = saveMisbehaviorWindow(currentUniqueWindow, 'focus');
        saveMisbehavior.then(function (response) {
            $('#cheatingAlert').modal('hide');
        });
        
    }

    /** Triggered if press F12 */
    // $(document).keydown(function (event) {
    //     if (event.keyCode == 123) {
    //         let saveMisbehavior = saveMisbehaviorWindow('f12', guid());
    //         saveMisbehavior.then(function (response) {
    //             $('#cheatingAlert').modal('show');
    //         });
    //         return false;
    //     } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
    //         let saveMisbehavior = saveMisbehaviorWindow('inspect', guid());
    //         saveMisbehavior.then(function (response) {
    //             $('#cheatingAlert').modal('show');
    //         });
    //         return false;
    //     }
    // });

    // /** Triggered if right click */
    // $(document).on("contextmenu", function (e) {
    //     let saveMisbehavior = saveMisbehaviorWindow('right click', guid());
    //     saveMisbehavior.then(function (response) {
    //         $('#cheatingAlert').modal('show');
    //     });
    //     return false;
    // });

    // /** Triggered if copy */
    // $(document).on("copy", function (e) {
    //     let saveMisbehavior = saveMisbehaviorWindow('copy', guid());
    //     saveMisbehavior.then(function (response) {
    //         $('#cheatingAlert').modal('show');
    //     });
    //     return false;
    // });
    
    /** Countdown time in second */
    let countDownDate = new Date("{{ \Carbon\Carbon::parse($testUser->test->end_test)->timezone('Asia/Jakarta')->format('Y-m-d\TH:i:s.uP') }}").getTime();
    let countdown = setInterval(function () {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        document.getElementById("countdown").innerHTML = hours + "hour(s) " + minutes + "minute(s) " + seconds + "second(s) "; //1 hour(s) 43 minute(s) 20 second(s)
        if (countDownDate <= 0) {
            clearInterval(countdown);
            window.location.reload();
            document.getElementById('countdown').innerHTML = 'Waktu Habis';
            /** Reload */
            document.getElementById('submitAnswer').click();
        }
    }, 1000);

    /** Call Function */
    parseHtmlFetchAbout();
    parseHtmlFetchQuestions(currentQuestion);

    /** input name on change: answer */
    $(document).on('change', '.selectAnswer', function () {
        if (currentQuestion == totalQuestion) {
            document.getElementById('nextQuestion').disabled = false;
            document.getElementById('nextQuestion').innerHTML = 'Submit';
        } else {
            document.getElementById('nextQuestion').disabled = false;
            document.getElementById('nextQuestion').innerHTML = 'Next';
        }

        if (currentQuestion > 1) {
            document.getElementById('prevQuestion').disabled = false;
        }
    });

    /** Next */
    document.getElementById('nextQuestion').addEventListener('click', function () {
        if (currentQuestion == totalQuestion) {
            $('#confirmSubmit').modal('show');
        } else {
            document.getElementsByClassName('preloader')[0].style.display = 'block';
            let number = currentQuestion + 1;
            let answer = null;
            if (document.querySelector('input[name="answer"]:checked')) {
                answer = document.querySelector('input[name="answer"]:checked').value;
            }
            let save = saveAnswer(currentQuestion, answer, 'next');
            save.then(function (response) {
                currentQuestion = number;
                parseHtmlFetchAbout();
                parseHtmlFetchQuestions(number);
            });
        }
    });
    
    /** Prev */
    document.getElementById('prevQuestion').addEventListener('click', function () {
        if (currentQuestion >= 1) {
            document.getElementsByClassName('preloader')[0].style.display = 'block';
            let number = currentQuestion - 1;
            let answer = null;
            if (document.querySelector('input[name="answer"]:checked')) {
                answer = document.querySelector('input[name="answer"]:checked').value;
            }
            let save = saveAnswer(currentQuestion, answer, 'prev');
            save.then(function (response) {
                currentQuestion = number;
                parseHtmlFetchAbout();
                parseHtmlFetchQuestions(number);
            });
        }
    });
});
</script>
@endsection

@section('content')
<div id="startQuestion">
    <div class="engage-toolbar d-flex position-fixed px-5 fw-bolder top-30 end-0 mt-20 gap-2" style="z-index: 9999;">
        <div class="card card-body bg-secondary">
            <p class="fw-bolder text-center">Camera</p>
            <video autoplay playsinline id="camera" class="w-150px h-150px"></video>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="card card-flush card-stretch-50 min-h-300px mb-10">
                <div class="card-body">
                    <p class="titleIterationQuestion fw-bolder"></p>
                    <div class="iterationQuestion rounded pt-4 pb-3 px-4 bg-secondary">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card card-flush card-stretch-50 min-h-300px mb-10">
                <div class="card-body">
                    <p class="fw-bolder mb-1">Name</p>
                    <p>{{ $testUser->test->name }}</p>
                    <p class="fw-bolder mb-1">Duration</p>
                    <p>{{ \Carbon\Carbon::parse($testUser->test->start_test)->diffInMinutes($testUser->test->end_test) }} Minutes</p>
                    <p class="fw-bolder mb-1">Time Left</p>
                    <p class="text-success fw-bolder" id="countdown"></p>
                    <p class="fw-bolder mb-1">Number of Questions</p>
                    <p>{{ $testUser->questions->count() }} Questions</p>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="card card-flush card-stretch-50 min-h-500px mb-7">
                <div class="card-body">
                    <h4 class="titleNoQuestion"></h4>
                    <div class="pb-6 pt-4 min-h-100px fs-3 question">
                        Loading...
                    </div>
                    <div class="separator separator-dashed my-3 mb-10"></div>
                    <form id="answerForm">
                        @csrf
                        <div class="contentAnswer"></div>
                    </form>
                </div>
            </div>
            <div class="d-flex justify-content-between">
                <button type="button" class="btn btn-secondary" id="prevQuestion">Previous</button>
                <button type="button" class="btn btn-primary" id="nextQuestion">Next</button>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" id="confirmSubmit">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="alert alert-dismissible d-flex flex-center flex-column py-10 px-10 px-lg-20 w-100">
                    <button type="button" class="position-absolute top-0 end-0 m-2 btn btn-icon btn-icon-primary" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                            </svg>
                        </span>
                    </button>
                    <span class="svg-icon svg-icon-5tx svg-icon-primary mb-5">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
                            <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
                            <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
                        </svg>
                    </span>
                    <div class="text-center text-dark">
                        <h1 class="fw-bolder mb-5">Confirmation</h1>
                        <div class="separator separator-dashed border-primary opacity-25 mb-5"></div>
                        <div class="mb-9">Are you sure to end the test?</div>
                        <div class="d-flex flex-center flex-wrap">
                            <a href="javascript::" class="btn btn-secondary m-2" data-bs-dismiss="modal" aria-label="Close">Cancel</a>
                            <form id="processFinished" method="POST" action="{{ Route('user.test.base-process', $testUser->token) }}">
                                @csrf
                            </form>
                            <button type="submit" class="btn btn-primary m-2" form="processFinished">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="turnOnCamera">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <p class="font-bolder">Click button below to activate your camera</p>
                    <button class="btn btn-dark btn-sm" id="checkCamera">Open Camera</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" id="cheatingAlert">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="alert alert-dismissible d-flex flex-center flex-column py-10 px-10 px-lg-20 w-100">
                <button type="button" class="position-absolute top-0 end-0 m-2 btn btn-icon btn-icon-danger" data-bs-dismiss="modal" aria-label="Close">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                        </svg>
                    </span>
                </button>
                <span class="svg-icon svg-icon-5tx svg-icon-danger mb-5">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
                        <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
                        <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
                    </svg>
                </span>
                <div class="text-center text-dark">
                    <h1 class="fw-bolder mb-5">Warning!</h1>
                    <div class="separator separator-dashed border-danger opacity-25 mb-5"></div>
                    <div class="mb-9">Misbehaviour is detected and will be reported</div>
                    <div class="d-flex flex-center flex-wrap">
                        <a href="javascript::;" class="btn btn-danger m-2" data-bs-dismiss="modal" aria-label="Close">I understand</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection