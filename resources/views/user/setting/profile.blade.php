@extends('layouts.appUser')

@section('script')
<script>
    $(document).ready(function() {
        @if(Auth::guard('user')->user()->detail && Auth::user()->detail->city)
        formCity('#province', '#city', {{ Auth::guard('user')->user()->detail->city->id }});
        @endif

        initTagify(document.querySelector("input[name='degree']"), {
            whitelist: [
                @foreach(\App\Models\UserDetail::dataEducation() as $value)
                "{{ $value }}",
                @endforeach
            ],
            maxTags: 1,
            dropdown: {
                classname: "tagify__inline__suggestions",
                enabled: 0,
            }
        });

        initTagify(document.querySelector("input[name='major']"), {
            whitelist: [
                @foreach($majors as $major)
                "{{ $major->name }}",
                @endforeach
            ],
            maxTags: 1,
            dropdown: {
                maxItems: 30,
                classname: "tagify__inline__suggestions",
                enabled: 0,
            }
        });

        initTagify(document.querySelector("input[name='school']"), {
            whitelist: [
                @foreach($schools as $school)
                "{{ $school->name }}",
                @endforeach
            ],
            maxTags: 1,
            dropdown: {
                maxItems: 30,
                classname: "tagify__inline__suggestions",
                enabled: 0,
            }
        });
    });
</script>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        @if(!Auth::user()->detail()->exists())
        <div class="alert bg-danger d-flex flex-column flex-sm-row w-100 p-5 mb-5">
            <span class="svg-icon svg-icon-2hx svg-icon-light me-4 mb-5 mb-sm-0">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <path opacity="0.3" d="M12 22C13.6569 22 15 20.6569 15 19C15 17.3431 13.6569 16 12 16C10.3431 16 9 17.3431 9 19C9 20.6569 10.3431 22 12 22Z" fill="white" />
                    <path d="M19 15V18C19 18.6 18.6 19 18 19H6C5.4 19 5 18.6 5 18V15C6.1 15 7 14.1 7 13V10C7 7.6 8.7 5.6 11 5.1V3C11 2.4 11.4 2 12 2C12.6 2 13 2.4 13 3V5.1C15.3 5.6 17 7.6 17 10V13C17 14.1 17.9 15 19 15ZM11 10C11 9.4 11.4 9 12 9C12.6 9 13 8.6 13 8C13 7.4 12.6 7 12 7C10.3 7 9 8.3 9 10C9 10.6 9.4 11 10 11C10.6 11 11 10.6 11 10Z" fill="white" />
                </svg>
            </span>
            <div class="d-flex flex-column text-light pe-0 pe-sm-10">
                <h4 class="mb-2 text-light">Attention</h4>
                <span>Please complete your profile to start the test. Ensure your data is valid with your National ID Card (KTP) to be held accountable.</span>
            </div>
        </div>
        @endif
    </div>
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header mt-5">
                <div class="card-title flex-column">
                    <h3 class="fw-bolder mb-1">My Profile</h3>
                </div>
            </div>
            <div class="card-body">
                <form method="POST" action="#" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <h4 class="mb-7 d-flex align-items-center">
                            <i class="far fa-user-circle fs-2 text-dark me-2"></i> Personal
                        </h4>
                        <div class="col-lg-6">
                            <div class="mb-7 fv-row">
                                <label class="form-label">Name</label>
                                <p>{{ Auth::user()->name }}</p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-7 fv-row">
                                <label class="form-label">Email</label>
                                <p>{{ Auth::user()->email }}</p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-7 fv-row">
                                <label class="form-label">Gender</label>
                                <select class="form-select @error('gender') is-invalid @enderror" name="gender" data-control="select2" data-placeholder="Select Gender">
                                    <option></option>
                                    <option value="Laki-laki" {{ (Auth::guard('user')->user()->detail ? Auth::guard('user')->user()->detail->gender : old('gender')) == 'Laki-laki' ? 'selected' : '' }}>Male</option>
                                    <option value="Perempuan" {{ (Auth::guard('user')->user()->detail ? Auth::guard('user')->user()->detail->gender : old('gender')) == 'Perempuan' ? 'selected' : '' }}>Female</option>
                                </select>
                                @error('gender')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-7 fv-row">
                                <label class="form-label">Date of Birth</label>
                                <input type="text" class="form-control mb-2 @error('dob') is-invalid @enderror" name="dob" placeholder="Date of Birth" data-inputmask="'mask': '99/99/9999'" value="{{ Auth::guard('user')->user()->detail ? Auth::guard('user')->user()->detail->dob->format('d/m/Y') : old('dob') }}">
                                <div class="text-muted fs-7 mt-2">Format: DD/MM/YYYY</div>
                                @error('dob')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="mb-7 fv-row">
                                <label class="form-label">Phone Number</label>
                                <input type="text" class="form-control mb-2 @error('phone') is-invalid @enderror" name="phone" placeholder="Nomor Handphone" value="{{ Auth::guard('user')->user()->detail ? Auth::guard('user')->user()->detail->phone : old('phone') }}">
                                @error('phone')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <h4 class="mt-10 mb-7 d-flex align-items-center">
                            <i class="far fa-map fs-2 text-dark me-2"></i> Domicile
                        </h4>
                        <div class="col-lg-6">
                            <div class="mb-7 fv-row">
                                <label class="form-label">Province</label>
                                <select class="form-select @error('province') is-invalid @enderror" name="province" id="province" data-control="select2" data-placeholder="Select Province" onchange="formCity(this, '#city')">
                                    <option></option>
                                    @foreach($provinces as $province)
                                    <option value="{{ $province->id }}" {{ (Auth::guard('user')->user()->detail ? Auth::guard('user')->user()->detail->province->name : old('province')) == $province->name ? 'selected' : '' }}>{{ $province->name }}</option>
                                    @endforeach
                                </select>
                                @error('province')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-7 fv-row">
                                <label class="form-label">City</label>
                                <select class="form-select @error('city') is-invalid @enderror" name="city" id="city" data-control="select2" data-placeholder="Select City">
                                    <option></option>
                                </select>
                                @error('city')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="mb-7 fv-row">
                                <label class="form-label">Address</label>
                                <textarea class="form-control mb-2 @error('address') is-invalid @enderror" name="address" placeholder="Address">{{ Auth::guard('user')->user()->detail ? Auth::guard('user')->user()->detail->address : old('address') }}</textarea>
                                @error('address')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <h4 class="mt-10 mb-7 d-flex align-items-center">
                            <i class="fas fa-university fs-2 text-dark me-2"></i> Education
                        </h4>
                        <div class="col-lg-6">
                            <div class="mb-7 fv-row">
                                <label class="form-label">On Going/Final Education</label>
                                <input type="text" class="form-control mb-2 @error('degree') is-invalid @enderror" name="degree" placeholder="On Going/Final Education" value="{{ Auth::guard('user')->user()->detail ? Auth::guard('user')->user()->detail->degree : old('degree') }}">
                                @error('degree')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-7 fv-row">
                                <label class="form-label">Major</label>
                                <input type="text" class="form-control mb-2 @error('major') is-invalid @enderror" name="major" placeholder="Major" value="{{ Auth::guard('user')->user()->detail ? Auth::guard('user')->user()->detail->major : old('major') }}">
                                @error('major')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="mb-7 fv-row">
                                <label class="form-label">Name of School/University</label>
                                <input type="text" class="form-control mb-2 @error('school') is-invalid @enderror" name="school" placeholder="Name of School/University" value="{{ Auth::guard('user')->user()->detail ? Auth::guard('user')->user()->detail->school : old('school') }}">
                                @error('school')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="mt-4">
                                <button type="submit" class="btn btn-primary btn-sm px-10">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection