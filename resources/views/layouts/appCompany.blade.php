<!DOCTYPE html>
<html lang="en">
    <head>
        <title>StudioRank</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
        <link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    </head>

    <body
        data-kt-name="metronic"
        id="kt_app_body"
        data-kt-app-layout="light-sidebar"
        data-kt-app-sidebar-enabled="true"
        data-kt-app-sidebar-fixed="true"
        data-kt-app-sidebar-hoverable="true"
        data-kt-app-sidebar-push-header="true"
        data-kt-app-sidebar-push-toolbar="true"
        data-kt-app-sidebar-push-footer="true"
        data-kt-app-toolbar-enabled="true"
        class="app-default"
    >
        <div class="d-flex flex-column flex-root app-root" id="kt_app_root">
            <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
                <div id="kt_app_header" class="app-header">
                    <div class="app-container container-fluid d-flex align-items-stretch justify-content-between">
                        <div class="d-flex align-items-center d-lg-none ms-n2 me-2" title="Show sidebar menu">
                            <div class="btn btn-icon btn-active-color-primary w-35px h-35px" id="kt_app_sidebar_mobile_toggle">
                                <span class="svg-icon svg-icon-1">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="currentColor" />
                                        <path
                                            opacity="0.3"
                                            d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z"
                                            fill="currentColor"
                                        />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
                            <a href="javascript::" class="d-lg-none">
                                <img alt="Logo" src="{{ asset('logo.png') }}" class="h-30px" />
                            </a>
                        </div>
                        <div class="d-flex align-items-stretch justify-content-end flex-lg-grow-1" id="kt_app_header_wrapper">
                            <div
                                class="app-header-menu app-header-mobile-drawer align-items-stretch"
                                data-kt-drawer="true"
                                data-kt-drawer-name="app-header-menu"
                                data-kt-drawer-activate="{default: true, lg: false}"
                                data-kt-drawer-overlay="true"
                                data-kt-drawer-width="225px"
                                data-kt-drawer-direction="end"
                                data-kt-drawer-toggle="#kt_app_header_menu_toggle"
                                data-kt-swapper="true"
                                data-kt-swapper-mode="{default: 'append', lg: 'prepend'}"
                                data-kt-swapper-parent="{default: '#kt_app_body', lg: '#kt_app_header_wrapper'}"
                            >
                                <div class="menu menu-rounded menu-column menu-lg-row my-5 my-lg-0 align-items-stretch fw-semibold px-2 px-lg-0" id="kt_app_header_menu" data-kt-menu="true"></div>
                            </div>
                            <div class="app-navbar flex-shrink-0">
                                <div class="app-navbar-item ms-1 ms-lg-3" id="kt_header_user_menu_toggle">
                                    <a
                                        href="javascript::void();"
                                        class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary fw-bolder text-dark d-none d-md-block"
                                        data-kt-menu-trigger="click"
                                        data-kt-menu-attach="parent"
                                        data-kt-menu-placement="bottom-end"
                                    >
                                        <img src="{{ asset('assets/media/avatars/avatar.jpg') }}" alt="user" class="h-40px me-3 rounded-2" /> Hello, {{ auth()->guard('company')->user()->name }}!
                                    </a>
                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px" data-kt-menu="true">
                                        <div class="menu-item px-3">
                                            <div class="menu-content d-flex align-items-center px-3">
                                                <div class="symbol symbol-50px me-5">
                                                    <img alt="Logo" src="{{ asset('assets/media/avatars/avatar.jpg') }}" />
                                                </div>
                                                <div class="d-flex flex-column">
                                                    <div class="fw-bold d-flex align-items-center fs-5">{{ auth()->guard('company')->user()->name }}</div>
                                                    <a href="#" class="fw-semibold text-muted text-hover-primary fs-7">{{ auth()->guard('company')->user()->email }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="separator my-2"></div>
                                        <div class="menu-item px-5">
                                            <a href="{{ Route('company.auth.logout') }}" class="menu-link px-5">Sign Out</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="app-navbar-item d-lg-none ms-2 me-n3" title="Show header menu">
                                    <a href="javascript::void();" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                                        <img src="{{ asset('assets/media/avatars/avatar.jpg') }}" alt="user" class="h-40px me-3 rounded-2" />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
                    <div
                        id="kt_app_sidebar"
                        class="app-sidebar flex-column"
                        data-kt-drawer="true"
                        data-kt-drawer-name="app-sidebar"
                        data-kt-drawer-activate="{default: true, lg: false}"
                        data-kt-drawer-overlay="true"
                        data-kt-drawer-width="225px"
                        data-kt-drawer-direction="start"
                        data-kt-drawer-toggle="#kt_app_sidebar_mobile_toggle"
                    >
                        <div class="app-sidebar-logo px-6" id="kt_app_sidebar_logo">
                            <a href="{{ url('/') }}">
                                <img alt="Logo" src="{{ asset('logo.png') }}" class="h-25px app-sidebar-logo-default theme-light-show" />
                                <img alt="Logo" src="{{ asset('logo.png') }}" class="h-25px app-sidebar-logo-default theme-dark-show" />
                                <img alt="Logo" src="{{ asset('logo-simple.png') }}" class="h-25px app-sidebar-logo-minimize" />
                            </a>
                            <div
                                id="kt_app_sidebar_toggle"
                                class="app-sidebar-toggle btn btn-icon btn-shadow btn-sm btn-color-muted btn-active-color-primary body-bg h-30px w-30px position-absolute top-50 start-100 translate-middle rotate"
                                data-kt-toggle="true"
                                data-kt-toggle-state="active"
                                data-kt-toggle-target="body"
                                data-kt-toggle-name="app-sidebar-minimize"
                            >
                                <span class="svg-icon svg-icon-2 rotate-180">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            opacity="0.5"
                                            d="M14.2657 11.4343L18.45 7.25C18.8642 6.83579 18.8642 6.16421 18.45 5.75C18.0358 5.33579 17.3642 5.33579 16.95 5.75L11.4071 11.2929C11.0166 11.6834 11.0166 12.3166 11.4071 12.7071L16.95 18.25C17.3642 18.6642 18.0358 18.6642 18.45 18.25C18.8642 17.8358 18.8642 17.1642 18.45 16.75L14.2657 12.5657C13.9533 12.2533 13.9533 11.7467 14.2657 11.4343Z"
                                            fill="currentColor"
                                        />
                                        <path
                                            d="M8.2657 11.4343L12.45 7.25C12.8642 6.83579 12.8642 6.16421 12.45 5.75C12.0358 5.33579 11.3642 5.33579 10.95 5.75L5.40712 11.2929C5.01659 11.6834 5.01659 12.3166 5.40712 12.7071L10.95 18.25C11.3642 18.6642 12.0358 18.6642 12.45 18.25C12.8642 17.8358 12.8642 17.1642 12.45 16.75L8.2657 12.5657C7.95328 12.2533 7.95328 11.7467 8.2657 11.4343Z"
                                            fill="currentColor"
                                        />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div class="app-sidebar-menu overflow-hidden flex-column-fluid">
                            <div
                                id="kt_app_sidebar_menu_wrapper"
                                class="app-sidebar-wrapper hover-scroll-overlay-y my-5"
                                data-kt-scroll="true"
                                data-kt-scroll-activate="true"
                                data-kt-scroll-height="auto"
                                data-kt-scroll-dependencies="#kt_app_sidebar_logo, #kt_app_sidebar_footer"
                                data-kt-scroll-wrappers="#kt_app_sidebar_menu"
                                data-kt-scroll-offset="5px"
                                data-kt-scroll-save-state="true"
                            >
                                <div class="menu menu-column menu-rounded menu-sub-indention px-3" id="#kt_app_sidebar_menu" data-kt-menu="true" data-kt-menu-expand="false">
                                    <div class="menu-item">
                                        <div class="menu-content pb-2">
                                            <span class="menu-section text-muted text-uppercase fs-8 ls-1">MENU</span>
                                        </div>
                                    </div>
                                    <div class="menu-item">
                                        <a class="menu-link active" href="{{ Route('company.dashboard') }}">
                                            <span class="menu-icon">
                                                <i class="bi bi-grid fs-3 text-dark"></i>
                                            </span>
                                            <span class="menu-title fw-bolder">Dashboard</span>
                                        </a>
                                    </div>
                                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
                                        <span class="menu-link">
                                            <span class="menu-icon">
                                                <i class="bi bi-layers fs-3 text-dark"></i>
                                            </span>
                                            <span class="menu-title fw-bolder">Test Management</span>
                                            <span class="menu-arrow"></span>
                                        </span>
                                        <div class="menu-sub menu-sub-accordion">
                                            <div class="menu-item">
                                                <a class="menu-link" href="{{ Route('company.test.index') }}">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">List Test</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a class="menu-link" href="{{ Route('company.module.index') }}">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Modul</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a class="menu-link" href="{{ Route('company.grade.index') }}">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Ranking</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
                                        <span class="menu-link">
                                            <span class="menu-icon">
                                                <i class="bi bi-envelope fs-3 text-dark"></i>
                                            </span>
                                            <span class="menu-title fw-bolder">Email Blast</span>
                                            <span class="menu-arrow"></span>
                                        </span>
                                        <div class="menu-sub menu-sub-accordion">
                                            <div class="menu-item">
                                                <a class="menu-link" href="#">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">List</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a class="menu-link" href="#">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Konfigurasi</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div> -->
                                    <!-- <div class="menu-item">
                                        <a class="menu-link" href="#">
                                            <span class="menu-icon">
                                                <i class="bi bi-people fs-3 text-dark"></i>
                                            </span>
                                            <span class="menu-title fw-bolder">Pengguna</span>
                                        </a>
                                    </div> -->
                                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
                                        <span class="menu-link">
                                            <span class="menu-icon">
                                                <i class="bi bi-gear fs-3 text-dark"></i>
                                            </span>
                                            <span class="menu-title fw-bolder">Pengaturan</span>
                                            <span class="menu-arrow"></span>
                                        </span>
                                        <div class="menu-sub menu-sub-accordion">
                                            <div class="menu-item">
                                                <a class="menu-link" href="{{ Route('company.setting.account') }}">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Akun</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a class="menu-link" href="{{ Route('company.setting.company') }}">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Perusahaan</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                        <div class="d-flex flex-column flex-column-fluid">
                            <div id="kt_app_content" class="app-content flex-column-fluid py-3 py-lg-6">
                                <div id="kt_app_content_container" class="app-container container-xxl">
                                    @yield('content')
                                    <div class="modal fade" tabindex="-1" id="modalDelete">
                                        <div class="modal-dialog">
                                            <div class="modal-content bg-light-danger">
                                                <div class="modal-header">
                                                    <div class="text-center">
                                                        <h3 class="fw-bolder">Hapus Data</h3>
                                                    </div>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Apakah anda yakin ingin menghapus data? data yang dihapus tidak dapat dikembalikan.</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-outline btn-outline-danger btn-active-danger" data-bs-dismiss="modal">Batalkan</button>
                                                    <a href="" class="btn btn-danger" id="urlDelete">Hapus Sekarang</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            var hostUrl = "assets/";
        </script>
        <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
        <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
        <script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
        <script src="{{ asset('assets/plugins/custom/formrepeater/formrepeater.bundle.js') }}"></script>
        <script src="{{ asset('assets/js/appCompany.js') }}"></script>

        @yield('script')

        <script>
			@if(session('success'))
			toastr.success("{{ session('success') }}");
			@endif

			@if(session('error'))
			toastr.error("{{ session('error') }}");
			@endif
        </script>
    </body>
</html>
