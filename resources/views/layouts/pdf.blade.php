<!DOCTYPE html>
<html>

<head>
	<title>PDF</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="UTF-8">
	<link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css">
	<style>
		@media (min-width: 992px) {
			.wrapper {
				padding-left: 0px !important;
			}
		}
		
		@page {
			margin: 0;
		}

		body {
			background-color: #FFF !important;
		}

		section {
			width: 100vw;
			height: 100vh;
			page-break-after: always;
		}

		.page-break {
			page-break-before: always;
		}
	</style>
	@yield('style')
</head>

<body>
	<div class="d-flex flex-column flex-root">
		<div class="page d-flex flex-row flex-column-fluid">
			<div class="wrapper d-flex flex-column flex-row-fluid">
				<div class="content d-flex flex-column flex-column-fluid">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
</body>

</html>