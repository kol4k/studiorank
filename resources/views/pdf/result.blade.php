@extends('layouts.pdf')

@section('content')
<section id="f">
    <div class="d-flex flex-column flex-center bg-primary" style="height: 100%;">
        <div>
            <span class="h1 text-light">ASSESSMENT REPORT</span><br>
            <span class="h3 text-light text-uppercase">{{ $testUser->name }}</span>
        </div>
        <div class="position-absolute justify-content-center align-items-center top-0 bg-light rounded-3 p-10" style="margin-top: -10px;">
            <div class="bg-light" style="width: 500px; text-align: center;">
                <img src="{{ asset('header-pdf.png') }}" width="300px"/>
            </div>
        </div>
        <div class="text-light position-absolute bottom-0" style="margin-bottom: 20px">
            <img src="{{ asset('logo-white.png') }}" width="200px">
            <img src="{{ asset('techready-white.png') }}" width="200px">
        </div>
    </div>
</section>
<section>
    <div class="d-flex flex-column bg-primary position-absolute left-0 p-10" style="height: 100%">&nbsp;</div>
    <div class="container">
        <div class="row" style="padding-left: 60px;">
            <div class="col-lg-12">
                <div class="d-flex justify-content-between pt-5 align-items-center">
                    <h2 class="text-muted fw-boldest text-uppercase">Candidate Information</h2>
                    <div class="text-muted">{{ $testUser->name }} | {{ $testUser->test->name }}</div>
                </div>
                <div class="separator separator-dashed border-2 border-muted mt-2 mb-3"></div>
            </div>
            <div class="col-lg-12">
                <h1 class="fw-bolder pt-5 pb-3">Profile</h1>
                <table class="table gy-2">
                    <tbody>
                        <tr>
                            <td class="fw-bolder w-200px">Full Name</td>
                            <td class="w-10px">:</td>
                            <td>{{ $testUser->name }}</td>
                        </tr>
                        <tr>
                            <td class="fw-bolder">Email</td>
                            <td>:</td>
                            <td>{{ $testUser->email }}</td>
                        </tr>
                        <tr>
                            <td class="fw-bolder">Phone Number</td>
                            <td>:</td>
                            <td>{{ $testUser->user->detail->phone }}</td>
                        </tr>
                        <tr>
                            <td class="fw-bolder">Date of Birth</td>
                            <td>:</td>
                            <td>{{ $testUser->user->detail->dob->format('d/m/Y') }}</td>
                        </tr>
                        <tr>
                            <td class="fw-bolder">Gender</td>
                            <td>:</td>
                            <td>{{ $testUser->user->detail->gender }}</td>
                        </tr>
                        <tr>
                            <td class="fw-bolder">Education Level</td>
                            <td>:</td>
                            <td>{{ $testUser->user->detail->degree }}</td>
                        </tr>
                        <tr>
                            <td class="fw-bolder">Company/University</td>
                            <td>:</td>
                            <td>{{ $testUser->user->detail->school }}</td>
                        </tr>
                        <tr>
                            <td class="fw-bolder">Major/Role</td>
                            <td>:</td>
                            <td>{{ $testUser->user->detail->major }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="separator separator-dashed border-muted mt-2 mb-3"></div>
            <div class="col-lg-12">
                <h1 class="fw-bolder pt-5 pb-3">Program Information</h1>
                <table class="table gy-2">
                    <tbody>
                        <tr>
                            <td class="fw-bolder w-200px">Company/University</td>
                            <td class="w-10px">:</td>
                            <td>{{ $testUser->test->company ? $testUser->test->company->name : 'Red Hat Indonesia & Techready Community-Coworking' }}</td>
                        </tr>
                        <tr>
                            <td class="fw-bolder">Program</td>
                            <td>:</td>
                            <td>{{ $testUser->test->name }}</td>
                        </tr>
                        <tr>
                            <td class="fw-bolder">Date Time</td>
                            <td>:</td>
                            <td>{{ $testUser->test->start_test->format('d F Y h:i').' - '.$testUser->test->end_test->format('d F Y h:i') }}</td>
                        </tr>
                        <tr>
                            <td class="fw-bolder">Appointment Length</td>
                            <td>:</td>
                            <td>{{ \Carbon\Carbon::parse($testUser->test->start_test)->diffInMinutes($testUser->test->end_test) }} Menit</td>
                        </tr>
                        <tr>
                            <td class="fw-bolder">Number of Question</td>
                            <td>:</td>
                            <td>{{ $testUser->test->count_question }} Soal</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="separator separator-dashed border-muted mt-2 mb-3"></div>
            <div class="col-lg-12">
                <div class="text-center pt-5">
                    <div><span class="fw-bolder">Start Time:</span> {{ \Carbon\Carbon::parse($testUser->start_test)->format('d F Y H:i:s') }}</div>
                    <div><span class="fw-bolder">Submit Time:</span> {{ \Carbon\Carbon::parse($testUser->submit_test)->format('d F Y H:i:s') }}</div>
                    <div><span class="fw-bolder">Duration:</span> {{ \Carbon\Carbon::parse($testUser->start_test)->diff($testUser->submit_test)->format('%I Menit %S Detik') }}</div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="d-flex flex-column bg-primary position-absolute left-0 p-10" style="height: 100%">&nbsp;</div>
    <div class="container">
        <div class="row" style="padding-left: 60px;">
            <div class="col-lg-12">
                <div class="d-flex justify-content-between pt-5 align-items-center">
                    <h2 class="text-muted fw-boldest text-uppercase">Section Analysis</h2>
                    <div class="text-muted">{{ $testUser->name }} | {{ $testUser->test->name }}</div>
                </div>
                <div class="separator separator-dashed border-2 border-muted mt-2 mb-3"></div>
            </div>
            <div class="col-lg-12">
                <table class="table table-striped table-rounded border border-gray-300 table-row-bordered table-row-gray-300 gy-2 gx-3 mt-5 mb-10">
                    <thead>
                        <tr>
                            <th class="fw-bolder w-100px">Ratings</th>
                            <th class="fw-bolder w-100px">Score</th>
                            <th class="fw-bolder">Descriptive Statement</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Outstanding</td>
                            <td>90 - 100</td>
                            <td>Applicant fully addresses all aspects of the criterion, convincingly demonstrates that it will meet the performance requirements, and demonstrates no weaknesses.</td>
                        </tr>
                        <tr>
                            <td>Good</td>
                            <td>61 - 89</td>
                            <td>Applicant fully addresses all aspects of the criterion, convincingly demonstrates a likelihood of meeting the requirements, and demonstrates only a few minor weaknesses. Any shortcomings are minor.</td>
                        </tr>
                        <tr>
                            <td>Adequate</td>
                            <td>41 - 60</td>
                            <td>Applicant addresses all aspects of the criterion and demonstrates the ability to meet the performance requirements. The Application may contain significant weaknesses and/or a number of minor shortcomings.</td>
                        </tr>
                        <tr>
                            <td>Fair</td>
                            <td>26 - 40</td>
                            <td>Applicant does not address all aspects of the criterion nor is evidence presented indicating the likelihood of successfully meeting the requirements. Significant weaknesses are demonstrated and clearly outweigh any strengths presented.</td>
                        </tr>
                        <tr>
                            <td>Poor</td>
                            <td>0 - 25</td>
                            <td>Applicant does not address all aspects of the criterion and the information presented indicates a strong likelihood of failure to meet the requirements.</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="d-flex flex-column bg-primary position-absolute left-0 p-10" style="height: 100%">&nbsp;</div>
    <div class="container">
        <div class="row" style="padding-left: 60px;">
            <div class="col-lg-12">
                <div class="d-flex justify-content-between pt-5 align-items-center">
                    <h2 class="text-muted fw-boldest text-uppercase">Section Analysis</h2>
                    <div class="text-muted">{{ $testUser->name }} | {{ $testUser->test->name }}</div>
                </div>
                <div class="separator separator-dashed border-2 border-muted mt-2 mb-3"></div>
            </div>
            <div class="col-lg-12">
                @foreach ($result as $key => $re)
                <table class="table table-striped table-rounded border border-gray-300 table-row-bordered table-row-gray-300 gy-2 gx-3 mt-5 mb-10">
                    <thead>
                        <tr>
                            <th class="fw-bolder w-150px">Category</th>
                            <th class="fw-bolder w-150px">Jumlah Soal</th>
                            <th class="fw-bolder w-150px">Nilai Jawaban Benar</th>
                            <th class="fw-bolder w-150px">Nilai Jawaban Salah</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $key }}</td>
                            <td>{{ $re['total'] }}</td>
                            <td>{{ $re['correct'] }}</td>
                            <td>{{ $re['incorrect'] }}</td>
                        </tr>
                        <tr>
                            <td class="fw-bolder">Hasil</td>
                            <td colspan="3">
                                <span class="badge fw-bolder mb-3" style="{{ $re['grade_color'] }}">{{ $re['grade_name'] }}</span>
                                <p>{{ $re['grade_description'] }}</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                @endforeach
            </div>
        </div>
    </div>
</section>
<section>
    <div class="d-flex flex-column bg-primary position-absolute left-0 p-10" style="height: 100%">&nbsp;</div>
    <div class="container">
        <div class="row" style="padding-left: 60px;">
            <div class="col-lg-12">
                <div class="d-flex justify-content-between pt-5 align-items-center">
                    <h2 class="text-muted fw-boldest text-uppercase">Activity Report</h2>
                    <div class="text-muted">{{ $testUser->name }} | {{ $testUser->test->name }}</div>
                </div>
                <div class="separator separator-dashed border-2 border-muted mt-2 mb-8"></div>
            </div>
            @foreach($testUser->questions->take(12) as $question)
            @if (isset($question->img_camera_base_64))
            <div class="col-sm-3 text-center">
                <img src="{{ $question->img_camera_base_64 }}" class="w-150px rounded-2 mb-3"><br>
                {{ $question->updated_at }}
            </div>
            @endif
            @endforeach
            <div class="separator separator-dashed border-2 border-muted mt-7 mb-8"></div>
            <div class="col-lg-12">
                <h1 class="fw-bolder pb-3">Misbehavior Indicators</h1>
                <p>This indicator is set based based on the duration and frequency with wich the candidate is in another tab/application while taking the test.</p>
            </div>
            <div class="col-sm-8">
                <table class="table table-striped table-rounded border border-gray-300 table-row-bordered table-row-gray-300 gy-2 mt-5">
                    <thead>
                        <tr>
                            <th class="fw-bolder text-center">Durasi</th>
                            <th class="fw-bolder text-center">Frekuensi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">0 detik - 15 detik</td>
                            <td class="text-center">{{ $misbehaviorWindow[0] }} kali</td>
                        </tr>
                        <tr>
                            <td class="text-center">15 detik - 30 detik</td>
                            <td class="text-center">{{ $misbehaviorWindow[1] }} kali</td>
                        </tr>
                        <tr>
                            <td class="text-center">30 detik - 1 menit</td>
                            <td class="text-center">{{ $misbehaviorWindow[2] }} kali</td>
                        </tr>
                        <tr>
                            <td class="text-center">> 1 menit</td>
                            <td class="text-center">{{ $misbehaviorWindow[3] }} kali</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@endsection