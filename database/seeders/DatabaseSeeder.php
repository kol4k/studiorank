<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $company = \App\Models\Company::create([
            'name' => 'Studio Rank'
        ]);

        \App\Models\UserCompany::create([
            'name' => 'Admin',
            'email' => 'admin@studiorank.xyz',
            'password' => bcrypt('12345678'),
            'company_id' => $company->id,
        ]);

        \App\Models\User::create([
            'name' => 'Pengguna 1',
            'email' => 'pengguna@studiorank.xyz',
            'password' => bcrypt('12345678'),
        ]);
    }
}
