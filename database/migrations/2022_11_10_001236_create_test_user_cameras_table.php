<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_user_cameras', function (Blueprint $table) {
            $table->id();
            $table->foreignId('test_user_id')->constrained('test_user')->onDelete('cascade');
            $table->foreignId('test_user_question_id')->constrained('test_user_question')->onDelete('cascade');
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_user_cameras');
    }
};
