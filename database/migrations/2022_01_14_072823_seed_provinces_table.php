<?php

use App\Models\Country;
use App\Models\Province;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SeedProvincesTable extends Migration
{
    public function __construct()
    {
        $this->source = '/../sources/csv/provinces.csv';
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $countryID = Country::where('code', 'ID')->first()->id;
        $source = dirname(__FILE__) . $this->source;
        $row = 0;
        if (($handle = fopen($source, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) {
                if ($row > 0) {
                    $province = new Province();
                    $province->id = $data[0];
                    $province->country_id = $countryID;
                    $province->name = ucwords(strtolower($data[1]));
                    $province->save();
                }
                $row++;
            }
        }

        $dkiJakarta = Province::where('name', 'Dki Jakarta')->update(['name' => 'DKI Jakarta']);
        $DiYogyakarta = Province::where('name', 'Di Yogyakarta')->update(['name' => 'DI Yogyakarta']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
