<?php

use App\Models\Country;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SeedCountriesTable extends Migration
{
    public function __construct()
    {
        $this->source = '/../sources/csv/countries.csv';
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $source = dirname(__FILE__) . $this->source;
        $row = 0;
        if (($handle = fopen($source, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 100000, ";")) !== FALSE) {
                if ($row > 0) {
                    $country = new Country();
                    $country->code = $data[0];
                    $country->latitude = $data[1] ? $data[1] : null;
                    $country->longitude = $data[2] ? $data[2] : null;
                    $country->name = $data[3];
                    $country->save();
                }
                $row++;
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
