<?php

use App\Models\Country;
use App\Models\City;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SeedCitiesTable extends Migration
{
    public function __construct()
    {
        $this->source = '/../sources/csv/cities.csv';
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $countryID = Country::where('code', 'ID')->first()->id;
        $source = dirname(__FILE__) . $this->source;
        $row = 0;
        if (($handle = fopen($source, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) {
                if ($row > 0) {
                    $city = new City();
                    $city->id = $data[0];
                    $city->country_id = $countryID;
                    $city->province_id = $data[1];
                    $city->name = ucwords(strtolower($data[2]));
                    $city->save();
                }
                $row++;
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
