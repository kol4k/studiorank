<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_detail', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->string('phone')->nullable();
            $table->string('gender')->nullable();
            $table->foreignId('province_id')->constrained('provinces')->onDelete('cascade')->nullable();
            $table->foreignId('city_id')->constrained('cities')->onDelete('cascade')->nullable();
            $table->text('address')->nullable();
            $table->string('degree')->nullable();
            $table->string('major')->nullable();
            $table->string('school')->nullable();
            $table->date('dob')->nullable();
            $table->string('ktp')->nullable();
            $table->string('ktm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_detail');
    }
};
