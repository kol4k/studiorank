<?php

use App\Models\School;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SeedSchoolsTable extends Migration
{
    public function __construct()
    {
        $this->source = '/../sources/csv/schools.csv';
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $source = dirname(__FILE__) . $this->source;
        $row = 0;
        if (($handle = fopen($source, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 100000, ";")) !== FALSE) {
                if ($row > 0) {
                    $school = new School();
                    $school->name = $data[0];
                    $school->save();
                }
                $row++;
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
