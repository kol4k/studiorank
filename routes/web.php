<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'rslt'], function () {
    Route::get('/{token}', [App\Http\Controllers\Pdf\ResultController::class, 'result'])->name('result');
    Route::get('/download/{token}', [App\Http\Controllers\Pdf\ResultController::class, 'downloadByBrowsershot'])->name('download');
});

/** User Route */
Route::group((env('APP_ENV') == 'local' || env('APP_ENV') == 'development' ? ['prefix' => '/'] : ['domain' => 'app.studiorank.xyz']), function () {
    Route::get('login', [App\Http\Controllers\User\AuthController::class, 'login'])->name('user.auth.login');
    Route::post('login', [App\Http\Controllers\User\AuthController::class, 'loginProcess'])->name('user.auth.login-process');
    Route::get('download', [App\Http\Controllers\FileController::class, 'getFile'])->name('user.download');

    /** Public API */
    Route::group(['prefix' => 'api'], function () {
        Route::get('cities', [App\Http\Controllers\User\Api\PublicController::class, 'apiCities'])->name('user.api.cities');
    });

    Route::group(['middleware' => ['auth:user']], function () {
        Route::get('/', [App\Http\Controllers\User\DashboardController::class, 'index'])->name('user.dashboard');
        Route::get('logout', [App\Http\Controllers\User\AuthController::class, 'logout'])->name('user.auth.logout');

        Route::group(['prefix' => 'test', 'middleware' => ['user.complete_data']], function () {
            Route::get('/', [App\Http\Controllers\User\TestController::class, 'index'])->name('user.test.index');

            Route::get('detail/{token}', [App\Http\Controllers\User\TestController::class, 'detail'])->name('user.test.detail');

            Route::group(['middleware' => ['user.test_finished']], function () {
                Route::get('start/{token}', [App\Http\Controllers\User\TestController::class, 'start'])->name('user.test.start');
                Route::post('start/{token}', [App\Http\Controllers\User\TestController::class, 'startProcess'])->name('user.test.start-process');
                Route::get('base/{token}', [App\Http\Controllers\User\TestController::class, 'base'])->name('user.test.base');
                Route::post('base/{token}', [App\Http\Controllers\User\TestController::class, 'baseProcess'])->name('user.test.base-process');
                Route::get('result/{token}', [App\Http\Controllers\User\TestController::class, 'result'])->name('user.test.result');
    
                Route::group(['prefix' => 'question'], function () {
                    Route::get('about/{token}', [App\Http\Controllers\User\Api\TestController::class, 'aboutQuestions'])->name('user.test.about.get');
                    Route::get('get/{token}', [App\Http\Controllers\User\Api\TestController::class, 'getQuestions'])->name('user.test.question.get');
                    Route::post('answer/{token}', [App\Http\Controllers\User\Api\TestController::class, 'saveAnswer'])->name('user.test.question.answer');
                    Route::post('misbehavior-window/{token}', [App\Http\Controllers\User\Api\TestController::class, 'misbehaviorWindow'])->name('user.test.question.misbehavior-window');
                });
            });
        });

        Route::group(['prefix' => 'setting'], function () {
            Route::get('profile', [App\Http\Controllers\User\SettingController::class, 'profile'])->name('user.setting.profile');
            Route::post('profile', [App\Http\Controllers\User\SettingController::class, 'profileProcess'])->name('user.setting.profile-process');
        });
    });
});

/** Company Route */
Route::group((env('APP_ENV') == 'local' || env('APP_ENV') == 'development' ? ['prefix' => 'company'] : ['domain' => 'admin.studiorank.xyz']), function () {

    /** Download File */    
    Route::get('/', [App\Http\Controllers\Company\DashboardController::class, 'index'])->name('company.home')->middleware('auth:company');
    Route::get('login', [App\Http\Controllers\Company\AuthController::class, 'login'])->name('company.auth.login');
    Route::post('login', [App\Http\Controllers\Company\AuthController::class, 'loginProcess'])->name('company.auth.login-process');

    Route::group(['middleware' => ['auth:company']], function () {
        Route::get('dashboard', [App\Http\Controllers\Company\DashboardController::class, 'index'])->name('company.dashboard');
        Route::get('logout', [App\Http\Controllers\Company\AuthController::class, 'logout'])->name('company.auth.logout');
        Route::get('download', [App\Http\Controllers\FileController::class, 'getFile'])->name('company.download');

        Route::group(['prefix' => 'test'], function () {
            Route::get('/', [App\Http\Controllers\Company\TestController::class, 'index'])->name('company.test.index');
            Route::get('{id}/detail', [App\Http\Controllers\Company\TestController::class, 'show'])->name('company.test.show');
            Route::get('create', [App\Http\Controllers\Company\TestController::class, 'create'])->name('company.test.create');
            Route::post('create', [App\Http\Controllers\Company\TestController::class, 'store'])->name('company.test.store');
            Route::get('{id}/edit', [App\Http\Controllers\Company\TestController::class, 'edit'])->name('company.test.edit');
            Route::post('{id}/edit', [App\Http\Controllers\Company\TestController::class, 'update'])->name('company.test.update');

            Route::group(['prefix' => 'invitation'], function () {
                Route::get('{id}/create', [App\Http\Controllers\Company\InvititationParticipantController::class, 'createSingle'])->name('company.test.invititation.create');
                Route::post('{id}/create', [App\Http\Controllers\Company\InvititationParticipantController::class, 'storeSingle'])->name('company.test.invititation.store');
                Route::get('{id}/create-multiple', [App\Http\Controllers\Company\InvititationParticipantController::class, 'createMultiple'])->name('company.test.invititation.create-multiple');
                Route::post('{id}/create-multiple', [App\Http\Controllers\Company\InvititationParticipantController::class, 'storeMultiple'])->name('company.test.invititation.store-multiple');
            });
        });

        Route::group(['prefix' => 'module'], function () {
            Route::get('/', [App\Http\Controllers\Company\ModuleController::class, 'index'])->name('company.module.index');
            Route::get('{id}/detail', [App\Http\Controllers\Company\ModuleController::class, 'show'])->name('company.module.show');
            Route::get('create', [App\Http\Controllers\Company\ModuleController::class, 'create'])->name('company.module.create');
            Route::post('store', [App\Http\Controllers\Company\ModuleController::class, 'store'])->name('company.module.store');
            Route::post('edit/{id}', [App\Http\Controllers\Company\ModuleController::class, 'update'])->name('company.module.update');
            Route::get('delete/{id}', [App\Http\Controllers\Company\ModuleController::class, 'destroy'])->name('company.module.destroy');

            Route::group(['prefix' => 'question'], function () {
                Route::get('create/{id}', [App\Http\Controllers\Company\QuestionController::class, 'createSingle'])->name('company.module.question.create');
                Route::post('store/{id}', [App\Http\Controllers\Company\QuestionController::class, 'storeSingle'])->name('company.module.question.store');
                Route::get('create-multiple/{id}', [App\Http\Controllers\Company\QuestionController::class, 'createMultiple'])->name('company.module.question.create-multiple');
                Route::post('create-multiple/{id}', [App\Http\Controllers\Company\QuestionController::class, 'storeMultiple'])->name('company.module.question.store-multiple');
                Route::get('edit/{id}', [App\Http\Controllers\Company\QuestionController::class, 'edit'])->name('company.module.question.edit');
                Route::post('edit/{id}', [App\Http\Controllers\Company\QuestionController::class, 'update'])->name('company.module.question.update');
                Route::get('delete/{id}', [App\Http\Controllers\Company\QuestionController::class, 'destroy'])->name('company.module.question.destroy');
            });
        });

        Route::group(['prefix' => 'grade'], function () {
            Route::get('/', [App\Http\Controllers\Company\GradeController::class, 'index'])->name('company.grade.index');
            Route::get('create', [App\Http\Controllers\Company\GradeController::class, 'create'])->name('company.grade.create');
            Route::post('create', [App\Http\Controllers\Company\GradeController::class, 'store'])->name('company.grade.store');
            Route::get('edit/{id}', [App\Http\Controllers\Company\GradeController::class, 'edit'])->name('company.grade.edit');
            Route::post('edit/{id}', [App\Http\Controllers\Company\GradeController::class, 'update'])->name('company.grade.update');
            Route::get('delete/{id}', [App\Http\Controllers\Company\GradeController::class, 'destroy'])->name('company.grade.destroy');
        });

        Route::group(['prefix' => 'setting'], function () {
            Route::get('account', [App\Http\Controllers\Company\SettingController::class, 'account'])->name('company.setting.account');
            Route::post('account', [App\Http\Controllers\Company\SettingController::class, 'accountUpdate'])->name('company.setting.account-update');
            Route::post('password', [App\Http\Controllers\Company\SettingController::class, 'passwordUpdate'])->name('company.setting.password-update');
            Route::get('company', [App\Http\Controllers\Company\SettingController::class, 'company'])->name('company.setting.company');
            Route::post('company', [App\Http\Controllers\Company\SettingController::class, 'companyUpdate'])->name('company.setting.company-update');
        });
    });
});